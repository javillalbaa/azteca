package com.app.azteca.azteca.Controladores.variablesConstant;

import android.util.Log;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClientHelper<T> {

    Retrofit retrofit;
    List<T> result;

    public RestClientHelper(){

         retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }

    public Retrofit getInstance(){
        return retrofit;
    }

    public List<T> Call(Call<GenericResponse<T>> llamada){

        Call<GenericResponse<T>> call = llamada;

        try {

            call.enqueue(new Callback<GenericResponse<T>>() {

                @Override
                public void onResponse(Call<GenericResponse<T>> call, Response<GenericResponse<T>> response) {

                    Log.d("sdfsfd",response.body().getResult().toString());
                }

                @Override
                public void onFailure(Call<GenericResponse<T>> call, Throwable t) {
                    Log.e("Error test:   ", t.toString());

                }
            });
        }
        catch (Exception ex){
            Exception ff = ex;

        }

        return result;
    }

}
