package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.Controladores.presenters.Result;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;

import java.util.List;

public class EventoAdpater extends BaseAdapter {
    private Context context;
    private List<Evento> eventos;

    public EventoAdpater(Context context, List<Evento> eventos) {
        this.context = context;
        this.eventos = eventos;
    }

    @Override
    public int getCount() {
        return eventos.size();
    }

    @Override
    public Evento getItem(int i) {
        return eventos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getEventoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        for (Evento evento:eventos) {
            //calendar.setDate();
        }
        return view;
    }
}
