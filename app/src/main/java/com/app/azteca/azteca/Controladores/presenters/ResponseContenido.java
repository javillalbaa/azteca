package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseContenido {

    @SerializedName("ContenidoID")
    @Expose
    private int contenidoID;
    @SerializedName("FileFileBankID")
    @Expose
    private int fileFileBankID;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("FilePath")
    @Expose
    private String filePath;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("UrlDocumento")
    @Expose
    private String urlDocumento;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseContenido() {
    }

    /**
     *
     * @param nombre
     * @param fileFileBankID
     * @param contenidoID
     * @param filePath
     * @param urlDocumento
     * @param descripcion
     */
    public ResponseContenido(int contenidoID, int fileFileBankID, String nombre, String filePath, String descripcion, String urlDocumento) {
        super();
        this.contenidoID = contenidoID;
        this.fileFileBankID = fileFileBankID;
        this.nombre = nombre;
        this.filePath = filePath;
        this.descripcion = descripcion;
        this.urlDocumento = urlDocumento;
    }

    public int getContenidoID() {
        return contenidoID;
    }

    public void setContenidoID(int contenidoID) {
        this.contenidoID = contenidoID;
    }

    public int getFileFileBankID() {
        return fileFileBankID;
    }

    public void setFileFileBankID(int fileFileBankID) {
        this.fileFileBankID = fileFileBankID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlDocumento() {
        return urlDocumento;
    }

    public void setUrlDocumento(String urlDocumento) {
        this.urlDocumento = urlDocumento;
    }

}
