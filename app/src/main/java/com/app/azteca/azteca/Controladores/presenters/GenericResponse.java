package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenericResponse<T> {

    @SerializedName("Result")
    @Expose
    private List<T> result = null;
    @SerializedName("Message")
    @Expose
    private Message message;

    /**
     * No args constructor for use in serialization
     */
    public GenericResponse() {
    }

    /**
     * @param message
     * @param result
     */
    public GenericResponse(List<T> result, Message message) {
        super();
        this.result = result;
        this.message = message;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}