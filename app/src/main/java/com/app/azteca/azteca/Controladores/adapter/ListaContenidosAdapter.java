package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Contenido;
import com.app.azteca.azteca.Controladores.presenters.Usuario;
import com.app.azteca.azteca.R;

import java.util.List;

public class ListaContenidosAdapter extends BaseAdapter {

    private Context context;
    private List<Contenido> contenidos;

    public ListaContenidosAdapter(Context context, List<Contenido> contenidos) {
        this.context = context;
        this.contenidos = contenidos;
    }

    @Override
    public int getCount() {
        return contenidos.size();
    }

    @Override
    public Contenido getItem(int i) {
        return contenidos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getParametroID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_usuario, viewGroup, false);
        }

        TextView nombre = view.findViewById(R.id.textviewNombreUsuario);
        TextView correo =view.findViewById(R.id.textviewCorreoUsuario);
        TextView telefono = view.findViewById(R.id.textviewTelefonoUsuario);
        final Contenido item = getItem(i);

        nombre.setText(Html.fromHtml( "<b>"+item.getTipoContenido()+"-"+item.getCategoriaContenido()+"</b>"));
        correo.setText(item.getDescripcion());
        telefono.setVisibility(View.INVISIBLE);
        return view;
    }
}
