package com.app.azteca.azteca.Controladores.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.azteca.azteca.R;


public class ListaPostViewHolder extends RecyclerView.ViewHolder {


    TextView txv_postHijoAutor, txv_postHijoFecha, txv_subpost;

    Button btnComentarPost;
    EditText commetField;


    public ListaPostViewHolder(View itemView) {
        super(itemView);

        //commetField = (EditText) itemView.findViewById(R.id.txt_ComentarioForo);
        //btnComentarPost = (Button) itemView.findViewById(R.id.btn_foroComment);
        txv_postHijoAutor = (TextView) itemView.findViewById(R.id.txv_postHijoAutor);
        //txv_postHijoFecha = (TextView) itemView.findViewById(R.id.txv_postHijoFecha);
        txv_subpost = (TextView) itemView.findViewById(R.id.txv_subpost);

    }
}