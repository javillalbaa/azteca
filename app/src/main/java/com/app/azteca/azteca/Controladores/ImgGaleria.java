package com.app.azteca.azteca.Controladores;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.R;
import com.bumptech.glide.Glide;

public class ImgGaleria extends AppCompatActivity implements SimpleGestureFilter.SimpleGestureListener {

    private SimpleGestureFilter detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_galeria);

        String img = getIntent().getExtras().getString("img");
        String desc = getIntent().getExtras().getString("desc");
        ImageView imagen = (ImageView) findViewById(R.id.img_galeria);
        TextView descripcion = (TextView) findViewById(R.id.text_galeria);
        Glide.with(getApplicationContext()).load(img).into(imagen);
        descripcion.setText(desc);

        detector = new SimpleGestureFilter(this,this);

    }

    public void Volver(View view) {
        finish();
    }
    public void Volver() {
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case SimpleGestureFilter.SWIPE_LEFT :
                Volver();
                break;
            case SimpleGestureFilter.SWIPE_RIGHT :
                Volver();
                break;
        }
    }

    @Override
    public void onDoubleTap() {
        //Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

}
