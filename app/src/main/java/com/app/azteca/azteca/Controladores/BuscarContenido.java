package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.CompartirCarroAdapter;
import com.app.azteca.azteca.Controladores.adapter.ListaContenidosAdapter;
import com.app.azteca.azteca.Controladores.presenters.Contenido;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;
import com.app.azteca.azteca.Controladores.services.BuscarContenidosService;
import com.app.azteca.azteca.Controladores.services.CategoriaService;
import com.app.azteca.azteca.Controladores.services.ContenidoByIdService;
import com.app.azteca.azteca.Controladores.services.ForoByIdService;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuscarContenido extends AppCompatActivity {
    TextView textView;
    ListView listView;
    EditText editText;
    ProgressBar simpleProgressBar;
    TextView textViewnoResults;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_usuarios);
        textView=findViewById(R.id.textviewbuscarusuarios);
        textViewnoResults=findViewById(R.id.textviewNoResults);
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbarBuscarUsuarios);
        toolbar.setTitle("Contenidos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView.setText("Buscar Contenidos");
        listView = findViewById(R.id.listviewBuscarUsuarios);
        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contenido cont= (Contenido) listView.getAdapter().getItem(i);
                switch (cont.getTipoContenido()){
                    case "Contenido":
                        getContenido(cont.getParametroID());
                        break;
                    case "Evento":
                        getEvento();
                        break;
                    case "Foro":
                        getForo(cont.getParametroID());
                        break;
                    case "Galeria":
                        getGaleria();
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(BuscarContenido.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(BuscarContenido.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getUsuarios(View view){
        editText = findViewById(R.id.edittextparametrobusqueda);
        simpleProgressBar.setVisibility(View.VISIBLE);
        editText.clearFocus();
        textViewnoResults.setVisibility(View.GONE);
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        BuscarContenidosService buscarUsuariosService = retrofit.create(BuscarContenidosService.class);
        Call<GenericResponse<Contenido>> call = buscarUsuariosService.getContenidos(constant.getToken(),editText.getText().toString());
        //Call<TokenResponse> call = serviceTuto.body(new LoginResponse("javier","s"));

        call.enqueue(new Callback<GenericResponse<Contenido>>() {

            @Override
            public void onResponse(Call<GenericResponse<Contenido>> call, Response<GenericResponse<Contenido>> response) {
                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        Log.d("Datos:",response.body().toString());
                        if(response.body().getResult().size()==0){
                            textViewnoResults.setVisibility(View.VISIBLE);
                        }else{

                        }
                        List<Contenido> contenidos= response.body().getResult();
                        ListaContenidosAdapter adapter= new ListaContenidosAdapter(getApplicationContext(),contenidos);
                        simpleProgressBar.setVisibility(View.GONE);
                        listView.setAdapter(adapter);
                    }else{
                        new Toast(getApplicationContext()).setText("Error cargando contenidos");
                    }
                }catch (Exception e){
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(BuscarContenido.this,
                            "No se pudo cargar los contenidos",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Contenido>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }

        });
    }
    public void getContenido(Integer id){
        simpleProgressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ContenidoByIdService categoriaService = retrofit.create(ContenidoByIdService.class);
        Call<GenericResponse<responseCategoria>> call = categoriaService.getContenido(constant.getToken(),id);

        call.enqueue(new Callback<GenericResponse<responseCategoria>>() {

            @Override
            public void onResponse(Call<GenericResponse<responseCategoria>> call, Response<GenericResponse<responseCategoria>> response) {
                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        Log.d("Datos:",response.body().toString());
                        List<responseCategoria> categoria= response.body().getResult();
                        Intent intent = new Intent(getApplicationContext(), InfoCategoria.class);
                        intent.putExtra("inf_categoria", (Serializable)categoria.get(0));
                        simpleProgressBar.setVisibility(View.GONE);
                        startActivity(intent);
                    }else{
                        new Toast(getApplicationContext()).setText("Error cargando noticia");
                    }
                }catch (Exception e){
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(BuscarContenido.this,
                            "No se pudo cargar las noticias",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<responseCategoria>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }

        });
    }
    public void getEvento(){
        Intent intent = new Intent(getApplicationContext(), Eventos.class);
        startActivity(intent);
    }
    public void getForo(Integer id){
        simpleProgressBar.setVisibility(View.VISIBLE);
        Gson gson = new GsonBuilder().setLenient().create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ForoByIdService categoriaService = retrofit.create(ForoByIdService.class);
        Call<GenericResponse<Foro>> call = categoriaService.getContenido(constant.getToken(),id);

        call.enqueue(new Callback<GenericResponse<Foro>>() {

            @Override
            public void onResponse(Call<GenericResponse<Foro>> call, Response<GenericResponse<Foro>> response) {
                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        Log.d("Datos:",response.body().toString());
                        final List<Foro> foro= response.body().getResult();
                        ForoPostService service = retrofit.create(ForoPostService.class);

                        Call<GenericResponse<ForoPost>> call1 = service.getData(constant.getToken(), foro.get(0).getForoID());

                        call1.enqueue(new Callback<GenericResponse<ForoPost>>() {

                            @Override
                            public void onResponse(Call<GenericResponse<ForoPost>> call, Response<GenericResponse<ForoPost>> response) {
                                try{
                                    if( response.body().getMessage().getCode() == 200 ) {
                                        ArrayList<ForoPost> listOfStrings = new ArrayList<ForoPost>(response.body().getResult().size());
                                        listOfStrings.addAll(response.body().getResult());

                                        Intent intent = new Intent(getApplicationContext(), ForoPosts.class);
                                        intent.putExtra("posts", (Serializable) listOfStrings);
                                        intent.putExtra("foroInfo", (Serializable) foro.get(0));
                                        simpleProgressBar.setVisibility(View.GONE);
                                        startActivity(intent);
                                    }else{
                                        new Toast(getApplicationContext()).setText("Error cargando comentarios");
                                    }
                                }catch (Exception e){
                                    simpleProgressBar.setVisibility(View.GONE);
                                    Toast.makeText(BuscarContenido.this,
                                            "No se pudo cargar los comentarios",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<GenericResponse<ForoPost>> call, Throwable t) {
                                Log.e("Error test:   ", t.toString());

                            }

                        });
                    }else{
                        new Toast(getApplicationContext()).setText("Error cargando foro");
                    }
                }catch (Exception e){
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(BuscarContenido.this,
                            "No se pudo cargar los foros",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<Foro>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }

        });
    }
    public void getGaleria(){
        Intent intent = new Intent(getApplicationContext(), Eventos.class);
        startActivity(intent);
    }
}
