package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class listaEventosAdapter extends BaseAdapter {

    private Context context;
    private List<Evento> eventos;

    public listaEventosAdapter(Context context, List<Evento> eventos) {
        this.context = context;
        this.eventos = eventos;
    }

    @Override
    public int getCount() {
        return eventos.size();
    }

    @Override
    public Evento getItem(int i) {
        return eventos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getEventoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_event_in_list, viewGroup, false);
        }

        TextView nombre = view.findViewById(R.id.nombreeventolista);
        TextView descripcion =view.findViewById(R.id.descripcioneventolista);
        TextView fecha = view.findViewById(R.id.fechaeventolista);
        TextView lugar = view.findViewById(R.id.lugareventolista);
        ImageView imagen = view.findViewById(R.id.imagenEventoLista);
        final Evento item = getItem(i);

        nombre.setText(item.getNombre());
        descripcion.setText(Html.fromHtml(constant.formaOpenHtml(item.getDescripcion())));
        fecha.setText(item.getFechaEvento().substring(0,10));
        lugar.setText("Lugar: "+item.getLugar());

        //Bitmap img= item.getContentType().equals("image/svg+xml")?constant.imageFromString(item.getArchivo(),100,50):constant.imageFromStringCategory(item.getArchivo());

        Glide.with(context).load(item.getArchivo()).into(imagen);
        //imagen.setImageBitmap(img);

        return view;
    }
}
