package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.ForoPostAdapter;
import com.app.azteca.azteca.Controladores.adapter.ListaForoAdapter;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.services.ListaForoService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.widget.AdapterView.*;

public class ListaForos extends AppCompatActivity {

    private ListView listview;
    private ArrayList<Foro> cont;
    private ListaForoAdapter listaForos;
    private ForoPostAdapter listaPosts;
    Retrofit retrofit;
    Foro foro;
    private ProgressBar simpleProgressBar;

    public ListaForos() {

        retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_foro);

        listview = (ListView) findViewById(R.id.listviewForo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Foros");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        simpleProgressBar.setVisibility(View.VISIBLE);

        getForos();

        listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                foro = (Foro) parent.getAdapter().getItem(position);
                ForoPostService service = retrofit.create(ForoPostService.class);
                Call<GenericResponse<ForoPost>> call = service.getData(constant.getToken(), foro.getForoID());

                call.enqueue(new Callback<GenericResponse<ForoPost>>() {

                    @Override
                    public void onResponse(Call<GenericResponse<ForoPost>> call, Response<GenericResponse<ForoPost>> response) {

                        try{
                            if( response.body().getMessage().getCode() == 200 ) {

                                ArrayList<ForoPost> listOfStrings = new ArrayList<ForoPost>(response.body().getResult().size());
                                listOfStrings.addAll(response.body().getResult());

                                Intent intent = new Intent(getApplicationContext(), ForoPosts.class);
                                intent.putExtra("posts", (Serializable) listOfStrings);
                                intent.putExtra("foroInfo", (Serializable) foro);
                                simpleProgressBar.setVisibility(View.GONE);
                                startActivity(intent);

                            }else{
                                simpleProgressBar.setVisibility(View.GONE);
                                new Toast(getApplicationContext()).setText("Error cargando foros disponibles");
                                Intent intent = new Intent(getApplicationContext(), Menu.class);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            simpleProgressBar.setVisibility(View.GONE);
                            Toast.makeText(ListaForos.this,
                                    "No se pudo cargar los foros",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<GenericResponse<ForoPost>> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }

                });
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(ListaForos.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(ListaForos.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getForos() {

        ListaForoService service = retrofit.create(ListaForoService.class);
        Call<GenericResponse<Foro>> call = service.getData(constant.getToken());
        call.enqueue(new Callback<GenericResponse<Foro>>() {

            @Override
            public void onResponse(Call<GenericResponse<Foro>> call, Response<GenericResponse<Foro>> response) {

                try{
                    if( response.body().getMessage().getCode() == 200 ) {

                        ArrayList<Foro> listOfStrings = new ArrayList<Foro>(response.body().getResult().size());
                        listOfStrings.addAll(response.body().getResult());
                        cont = listOfStrings;
                        listaForos = new ListaForoAdapter(getApplicationContext(), cont);
                        simpleProgressBar.setVisibility(View.GONE);
                        listview.setAdapter(listaForos);

                    }else{
                        simpleProgressBar.setVisibility(View.GONE);
                        new Toast(getApplicationContext()).setText("Error cargando foros disponibles");
                        Intent intent = new Intent(getApplicationContext(), Menu.class);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(ListaForos.this,
                            "No se pudo cargar los foros",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<Foro>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }

        });
    }

}
