package com.app.azteca.azteca.Controladores.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.ForoPosts;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForoPostAdapter extends RecyclerView.Adapter<ForoPostViewHolder> {

    private Context context;
    private List<ForoPost> posts;
    private ArrayList<ForoPost> ComentarioPosts;
    RecyclerView rv;
    Retrofit retrofit;
    private AdapterForoPostCallback adapertCallBack;
    Foro foro;
    private ViewGroup row;
    private int id_posts;

    public ForoPostAdapter(Context context, List<ForoPost> foros, Foro foro) {
        this.context = context;
        this.posts = foros;
        this.foro = foro;
        this.adapertCallBack = ((AdapterForoPostCallback) context);
        this.retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }


    @NonNull
    @Override
    public ForoPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_foropost, parent, false);
        return new ForoPostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ForoPostViewHolder holder, final int position) {

        holder.nametxt.setText(posts.get(position).getPost());
        holder.autortxt.setText(posts.get(position).getNombresCompletos());
        //holder.fechatxt.setText(posts.get(position).getFechaPost());
        holder.txtIdPost.setText(posts.get(position).getForoPostID().toString());

        ArrayList<ForoPost> listOfComments = new ArrayList<ForoPost>(posts.get(position).getPosts().size());
        listOfComments.addAll(posts.get(position).getPosts());
        ComentarioPosts = listOfComments;

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                row = (ViewGroup) v.getParent().getParent();
                id_posts = Integer.parseInt(((TextView) row.findViewById(R.id.txv_idpost)).getText().toString());
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder.buttonViewOption);
                //inflating menu from xml resource
                if (adapertCallBack.onCountVerComentariosCallback(id_posts) > 0) {
                    popup.inflate(R.menu.options_menu);
                } else {
                    popup.inflate(R.menu.options_menu_2);
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:
                                adapertCallBack.onClickVerComentariosCallback(
                                        id_posts,
                                        "<font color=#424242>" + ((TextView) row.findViewById(R.id.txv_autor)).getText().toString() + "</font> - " +
                                                "<font color=#424242>" + ((TextView) row.findViewById(R.id.nameTxt)).getText().toString() + "</font>"
                                );
                                break;
                            case R.id.menu2:

                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View mView = LayoutInflater.from(context).inflate(R.layout.comentario, row, false);
                                final EditText comentario = (EditText) mView.findViewById(R.id.comentario);
                                FloatingActionButton mLogin = (FloatingActionButton) mView.findViewById(R.id.btnLogin);
                                mBuilder.setView(mView);
                                final AlertDialog dialog = mBuilder.create();
                                dialog.show();
                                mLogin.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (!comentario.getText().toString().isEmpty()) {
                                            //simpleProgressBar.setVisibility(View.VISIBLE);
                                            crearComentario(comentario.getText().toString());
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(context,
                                                    "El comentario no puede ser vacio",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    private void crearComentario(String comentario) {

                                        ForoPost item = new ForoPost();
                                        item.setForoID(foro.getForoID());
                                        item.setForoPostPadre(id_posts);
                                        item.setUsuarioID(constant.getIdUser());
                                        item.setPost(comentario);

                                        ForoPostService service = retrofit.create(ForoPostService.class);

                                        Call<GenericResponse<Boolean>> call = service.create(constant.getToken(), item);

                                        call.enqueue(new Callback<GenericResponse<Boolean>>() {

                                            @Override
                                            public void onResponse(Call<GenericResponse<Boolean>> call, Response<GenericResponse<Boolean>> response) {

                                                Boolean b = response.body().getResult().get(0);
                                                ArrayList<Boolean> listOfStrings = new ArrayList<Boolean>(response.body().getResult().size());
                                                listOfStrings.addAll(response.body().getResult());
                                                adapertCallBack.onMethodCallback(true,
                                                        id_posts,
                                                        "<font color=#424242>" + ((TextView) row.findViewById(R.id.txv_autor)).getText().toString() + "</font> - " +
                                                                "<font color=#424242>" + ((TextView) row.findViewById(R.id.nameTxt)).getText().toString() + "</font>"
                                                );
                                            }

                                            @Override
                                            public void onFailure(Call<GenericResponse<Boolean>> call, Throwable t) {
                                                Log.e("Error test:   ", t.toString());

                                            }
                                        });
                                    }
                                });
                                break;
                        }
                        return false;
                    }
                });
                popup.show();

            }

        });
    }

    public static interface AdapterForoPostCallback {
        void onMethodCallback(boolean status_call, int idPost, String comentario);
        int onCountVerComentariosCallback(int idPost);
        void onClickVerComentariosCallback(int idPost, String comentario);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
}