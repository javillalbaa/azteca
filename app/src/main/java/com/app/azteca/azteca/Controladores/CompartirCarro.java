package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.app.azteca.azteca.Controladores.adapter.CompartirCarroAdapter;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseCompartirCarro;
import com.app.azteca.azteca.Controladores.services.CompartirCarroService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompartirCarro extends AppCompatActivity {

    private ListView listview;
    private ArrayList<responseCompartirCarro> com_carro;
    private CompartirCarroAdapter adapterCarro;
    private ProgressBar simpleProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compartir_carro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Rutas Disponibles");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        simpleProgressBar.setVisibility(View.VISIBLE);
        listview = (ListView) findViewById(R.id.listview);
        getDatos();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                responseCompartirCarro item = (responseCompartirCarro) parent.getItemAtPosition(position);
                redirectToView(item);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(CompartirCarro.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(CompartirCarro.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToView(responseCompartirCarro compartirCarro){

        Intent intent = new Intent(getApplicationContext(), InfCompartirCarro.class);
        intent.putExtra("infCompartirCarro", (Serializable)compartirCarro);
        startActivity(intent);

    }

    private void getDatos() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();

        CompartirCarroService service = retrofit.create(CompartirCarroService.class);
        Call<GenericResponse<responseCompartirCarro>> call = service.getDatos(constant.getToken());

        call.enqueue(new Callback<GenericResponse<responseCompartirCarro>>() {

            @Override
            public void onResponse(Call<GenericResponse<responseCompartirCarro>> call, Response<GenericResponse<responseCompartirCarro>> response) {

                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        adapterCarro = new CompartirCarroAdapter(getApplicationContext(), response.body().getResult());
                        listview.setAdapter(adapterCarro);
                        simpleProgressBar.setVisibility(View.GONE);
                    }else{
                        new Toast(getApplicationContext()).setText("Error cargando rutas disponibles");
                        Intent intent = new Intent(getApplicationContext(), Menu.class);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(CompartirCarro.this,
                            "No se pudo cargar las rutas disponibles",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<responseCompartirCarro>> call, Throwable t) {
                simpleProgressBar.setVisibility(View.GONE);
                Log.e("Error test:   ", t.toString());
            }

        });
    }

}
