package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.MenuResult;
import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import java.io.IOException;
import java.util.List;

public class MenuAdapter extends BaseAdapter{

    private Context context;
    private List<MenuResult> menu;

    public MenuAdapter(Context context, List<MenuResult> menu) {
        this.context = context;
        this.menu = menu;
    }

    @Override
    public int getCount() {
        return menu.size();
    }

    @Override
    public MenuResult getItem(int i) {
        return menu.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }


        ImageView imagen = (ImageView) view.findViewById(R.id.imagen);
        TextView nombre = (TextView) view.findViewById(R.id.nombre);
        final MenuResult item = getItem(i);
        Bitmap img = constant.imageFromString(item.getIcono(), 100, 100);
        imagen.setImageBitmap(img);
        nombre.setText(item.getNombre());
        return view;
    }
}
