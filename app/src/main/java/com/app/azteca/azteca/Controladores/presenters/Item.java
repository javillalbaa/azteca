package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("ImagenTemaID")
    @Expose
    private int imagenTemaID;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("Ruta")
    @Expose
    private String ruta;
    @SerializedName("TemaID")
    @Expose
    private int temaID;
    @SerializedName("UsuarioID")
    @Expose
    private int usuarioID;
    @SerializedName("Caption")
    @Expose
    private Object caption;
    @SerializedName("Archivo")
    @Expose
    private String archivo;
    @SerializedName("ContentType")
    @Expose
    private String contentType;
    @SerializedName("NombreTema")
    @Expose
    private String nombreTema;
    @SerializedName("FileFileBankID")
    @Expose
    private int fileFileBankID;
    @SerializedName("Existe")
    @Expose
    private boolean existe;

    /**
     * No args constructor for use in serialization
     */
    public Item() {
    }

    /**
     * @param fileFileBankID
     * @param nombreTema
     * @param archivo
     * @param temaID
     * @param descripcion
     * @param ruta
     * @param imagenTemaID
     * @param caption
     * @param existe
     * @param contentType
     * @param usuarioID
     */
    public Item(int imagenTemaID, String descripcion, String ruta, int temaID, int usuarioID, Object caption, String archivo, String contentType, String nombreTema, int fileFileBankID, boolean existe) {
        super();
        this.imagenTemaID = imagenTemaID;
        this.descripcion = descripcion;
        this.ruta = ruta;
        this.temaID = temaID;
        this.usuarioID = usuarioID;
        this.caption = caption;
        this.archivo = archivo;
        this.contentType = contentType;
        this.nombreTema = nombreTema;
        this.fileFileBankID = fileFileBankID;
        this.existe = existe;
    }

    public int getImagenTemaID() {
        return imagenTemaID;
    }

    public void setImagenTemaID(int imagenTemaID) {
        this.imagenTemaID = imagenTemaID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getTemaID() {
        return temaID;
    }

    public void setTemaID(int temaID) {
        this.temaID = temaID;
    }

    public int getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(int usuarioID) {
        this.usuarioID = usuarioID;
    }

    public Object getCaption() {
        return caption;
    }

    public void setCaption(Object caption) {
        this.caption = caption;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }

    public int getFileFileBankID() {
        return fileFileBankID;
    }

    public void setFileFileBankID(int fileFileBankID) {
        this.fileFileBankID = fileFileBankID;
    }

    public boolean isExiste() {
        return existe;
    }

    public void setExiste(boolean existe) {
        this.existe = existe;
    }

}
