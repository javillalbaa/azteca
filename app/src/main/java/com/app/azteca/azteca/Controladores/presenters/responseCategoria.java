package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class responseCategoria implements Serializable {

    @SerializedName("ContenidoID")
    @Expose
    private int contenidoID;
    @SerializedName("CategoriaContenidoID")
    @Expose
    private int categoriaContenidoID;
    @SerializedName("CategoriaContenido")
    @Expose
    private String categoriaContenido;
    @SerializedName("Titulo")
    @Expose
    private String titulo;
    @SerializedName("SubTitulo")
    @Expose
    private String subTitulo;
    @SerializedName("DescripcionCorta")
    @Expose
    private String descripcionCorta;
    @SerializedName("DescripcionLarga")
    @Expose
    private String descripcionLarga;
    @SerializedName("Archivo")
    @Expose
    private String archivo;
    @SerializedName("ContentType")
    @Expose
    private String contentType;
    @SerializedName("FechaInsercion")
    @Expose
    private String fechaInsercion;
    @SerializedName("FechaModificacion")
    @Expose
    private String fechaModificacion;
    @SerializedName("UsuarioPublica")
    @Expose
    private String usuariopublica;

    /**
     * No args constructor for use in serialization
     */
    public responseCategoria() {
    }

    public responseCategoria(int contenidoID, int categoriaContenidoID, String categoriaContenido, String titulo, String subTitulo, String descripcionCorta, String descripcionLarga, String archivo, String contentType, String fechaInsercion, String fechaModificacion, String usuariopublica) {
        this.contenidoID = contenidoID;
        this.categoriaContenidoID = categoriaContenidoID;
        this.categoriaContenido = categoriaContenido;
        this.titulo = titulo;
        this.subTitulo = subTitulo;
        this.descripcionCorta = descripcionCorta;
        this.descripcionLarga = descripcionLarga;
        this.archivo = archivo;
        this.contentType = contentType;
        this.fechaInsercion = fechaInsercion;
        this.fechaModificacion = fechaModificacion;
        this.usuariopublica = usuariopublica;
    }

    public String getUsuariopublica() {
        return usuariopublica;
    }

    public void setUsuariopublica(String usuariopublica) {
        this.usuariopublica = usuariopublica;
    }

    public int getContenidoID() {
        return contenidoID;
    }

    public void setContenidoID(int contenidoID) {
        this.contenidoID = contenidoID;
    }

    public int getCategoriaContenidoID() {
        return categoriaContenidoID;
    }

    public void setCategoriaContenidoID(int categoriaContenidoID) {
        this.categoriaContenidoID = categoriaContenidoID;
    }

    public String getCategoriaContenido() {
        return categoriaContenido;
    }

    public void setCategoriaContenido(String categoriaContenido) {
        this.categoriaContenido = categoriaContenido;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubTitulo() {
        return subTitulo;
    }

    public void setSubTitulo(String subTitulo) {
        this.subTitulo = subTitulo;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public void setDescripcionCorta(String descripcionCorta) {
        this.descripcionCorta = descripcionCorta;
    }

    public String getDescripcionLarga() {
        return descripcionLarga;
    }

    public void setDescripcionLarga(String descripcionLarga) {
        this.descripcionLarga = descripcionLarga;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
