package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListaEventos extends AppCompatActivity {
    private ListView listView;
    private Fragment fragmentEvents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_lista_eventos);
        Intent intent= getIntent();
        String date=  intent.getStringExtra("date");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Lista de eventos-"+date);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        List<Evento> eventos=(List<Evento>) intent.getSerializableExtra("eventos");

        Bundle bundle = new Bundle();
        bundle.putSerializable("eventos",(Serializable) eventos);

        if(eventos.size()>0){
            lista_eventos fragment = new lista_eventos();
            fragment.setArguments(bundle);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragmentListaEventos, fragment);
            ft.commit();
        }else{
            TextView textView= findViewById(R.id.textviewNoEventos);
            textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Intent intent = new Intent(ListaEventos.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            Toast.makeText(ListaEventos.this,
                                    response.body().getMessage().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
