package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Result;
import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class CategoriaAdapter extends BaseAdapter {

    private Context context;
    private List<responseCategoria> categoria;


    public CategoriaAdapter(Context context, List<responseCategoria> categoria) {
        this.context = context;
        this.categoria = categoria;
    }

    @Override
    public int getCount() {
        return categoria.size();
    }

    @Override
    public responseCategoria getItem(int i) {
        return categoria.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getContenidoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_categorias, viewGroup, false);
        }

        ImageView imagen = (ImageView) view.findViewById(R.id.imagen);
        TextView titulo = (TextView) view.findViewById(R.id.titulo);
        TextView desc_corta = (TextView) view.findViewById(R.id.desc_corta);

        final responseCategoria item = getItem(i);

        Glide.with(context).load(item.getArchivo()).into(imagen);
        titulo.setText(Html.fromHtml(item.getTitulo()));
        desc_corta.setText(Html.fromHtml(constant.formaOpenHtml(item.getDescripcionCorta())));

        return view;
    }
}
