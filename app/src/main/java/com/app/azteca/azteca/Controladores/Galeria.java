package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.TemasAdapter;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseTemas;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.services.TemasService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Galeria extends AppCompatActivity {

    private ListView listview;
    private ArrayList<responseTemas> cont;
    private TemasAdapter temas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_galeria);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listview = (ListView) findViewById(R.id.listview);
        getGaleria();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                responseTemas tema = (responseTemas) parent.getItemAtPosition(position);
                try {
                    redirectToView(tema);
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(Galeria.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(Galeria.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToView(responseTemas temas) throws NoSuchFieldException, IllegalAccessException {
        Intent intent = new Intent(getApplicationContext(), GaleriaTema.class);
        intent.putExtra("tema", temas.getTemaCatalogoID());
        intent.putExtra("tit", temas.getDescripcion());
        startActivity(intent);
    }

    private void getGaleria() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
        TemasService service = retrofit.create(TemasService.class);
        Call<GenericResponse<responseTemas>> call = service.getTemas(constant.getToken());
        call.enqueue(new Callback<GenericResponse<responseTemas>>() {

            @Override
            public void onResponse(Call<GenericResponse<responseTemas>> call, Response<GenericResponse<responseTemas>> response) {
                temas = new TemasAdapter(getApplicationContext(), response.body().getResult());
                listview.setAdapter(temas);
            }

            @Override
            public void onFailure(Call<GenericResponse<responseTemas>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

}
