package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class responseCompartirCarro implements Serializable {

    @SerializedName("CompartirCarroID")
    @Expose
    private int compartirCarroID;
    @SerializedName("FechaSalida")
    @Expose
    private String fechaSalida;
    @SerializedName("NombreContacto")
    @Expose
    private String nombreContacto;
    @SerializedName("TelefonoContacto")
    @Expose
    private String telefonoContacto;
    @SerializedName("Destino")
    @Expose
    private String destino;
    @SerializedName("HoraSalida")
    @Expose
    private String horaSalida;
    @SerializedName("PuntoEncuentro")
    @Expose
    private String puntoEncuentro;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("Activo")
    @Expose
    private boolean activo;
    @SerializedName("Bloquear")
    @Expose
    private boolean bloquear;
    @SerializedName("UsuarioAuditID")
    @Expose
    private int usuarioAuditID;
    @SerializedName("FechaInsercion")
    @Expose
    private String fechaInsercion;

    public responseCompartirCarro(int compartirCarroID, String fechaSalida, String nombreContacto, String telefonoContacto, String destino, String horaSalida, String puntoEncuentro, String descripcion, boolean activo, boolean bloquear, int usuarioAuditID, String fechaInsercion) {
        this.compartirCarroID = compartirCarroID;
        this.fechaSalida = fechaSalida;
        this.nombreContacto = nombreContacto;
        this.telefonoContacto = telefonoContacto;
        this.destino = destino;
        this.horaSalida = horaSalida;
        this.puntoEncuentro = puntoEncuentro;
        this.descripcion = descripcion;
        this.activo = activo;
        this.bloquear = bloquear;
        this.usuarioAuditID = usuarioAuditID;
        this.fechaInsercion = fechaInsercion;
    }

    public responseCompartirCarro() {
    }

    public int getCompartirCarroID() {
        return compartirCarroID;
    }

    public void setCompartirCarroID(int compartirCarroID) {
        this.compartirCarroID = compartirCarroID;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getPuntoEncuentro() {
        return puntoEncuentro;
    }

    public void setPuntoEncuentro(String puntoEncuentro) {
        this.puntoEncuentro = puntoEncuentro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean isBloquear() {
        return bloquear;
    }

    public void setBloquear(boolean bloquear) {
        this.bloquear = bloquear;
    }

    public int getUsuarioAuditID() {
        return usuarioAuditID;
    }

    public void setUsuarioAuditID(int usuarioAuditID) {
        this.usuarioAuditID = usuarioAuditID;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }
}
