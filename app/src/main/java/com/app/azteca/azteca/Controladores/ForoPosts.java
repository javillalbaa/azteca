package com.app.azteca.azteca.Controladores;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.ForoPostAdapter;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;

import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForoPosts extends AppCompatActivity implements ForoPostAdapter.AdapterForoPostCallback {

    RecyclerView rv;
    TextView tituloForo, tituloForoDesc;
    Retrofit retrofit;
    ArrayList<ForoPost> posts;
    Foro foro;
    private ProgressBar simpleProgressBar;

    public ForoPosts() {

        retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foro_posts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Foro");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);

        Intent i = getIntent();
        posts = (ArrayList<ForoPost>) i.getSerializableExtra("posts");
        foro = (Foro) i.getSerializableExtra("foroInfo");

        tituloForo = findViewById(R.id.txv_TituloForo);
        tituloForoDesc = findViewById(R.id.txv_TituloForoDesc);
        String TitForo = "<font color=#424242>" + foro.getTema() + "</font>";
        String TitForoDesc = "<font color=#9e9e9e>"+ foro.getDescripcion() + "</font>";
        tituloForo.setText(Html.fromHtml(TitForo));
        tituloForoDesc.setText(Html.fromHtml(TitForoDesc));

        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new ForoPostAdapter(ForoPosts.this, posts, foro));

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(ForoPosts.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(ForoPosts.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void crearComentario(View view) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ForoPosts.this);
        View mView = getLayoutInflater().inflate(R.layout.comentario, null);
        final EditText comentario = (EditText) mView.findViewById(R.id.comentario);
        FloatingActionButton mLogin = (FloatingActionButton) mView.findViewById(R.id.btnLogin);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!comentario.getText().toString().isEmpty()){
                    simpleProgressBar.setVisibility(View.VISIBLE);
                    crearComentarioApi(comentario.getText().toString());
                    dialog.dismiss();
                }else{
                    Toast.makeText(ForoPosts.this,
                            "El comentario no puede ser vacio",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void crearComentarioApi(String comentario){

        ForoPost item = new ForoPost();
        item.setForoID(foro.getForoID());
        item.setForoPostPadre(0);
        item.setPost(comentario);
        item.setUsuarioID(constant.getIdUser());

        ForoPostService service = retrofit.create(ForoPostService.class);
        Call<GenericResponse<Boolean>> call = service.create(constant.getToken(), item);
        call.enqueue(new Callback<GenericResponse<Boolean>>() {

            @Override
            public void onResponse(Call<GenericResponse<Boolean>> call, Response<GenericResponse<Boolean>> response) {

                Boolean b = response.body().getResult().get(0);
                if (b) {
                    simpleProgressBar.setVisibility(View.GONE);
                    reloadComments(false, 0, "");
                }else {
                    simpleProgressBar.setVisibility(View.GONE);
                    new Toast(ForoPosts.this).setText("Error creando comentarío");
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Boolean>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }
        });
    }

    private void reloadComments(final Boolean status_call,final int idPost, final String comentario) {

        simpleProgressBar.setVisibility(View.VISIBLE);
        ForoPostService service = retrofit.create(ForoPostService.class);
        Call<GenericResponse<ForoPost>> call = service.getData(constant.getToken(), foro.getForoID());
        call.enqueue(new Callback<GenericResponse<ForoPost>>() {

            @Override
            public void onResponse(Call<GenericResponse<ForoPost>> call, Response<GenericResponse<ForoPost>> response) {

                try{
                    if( response.body().getMessage().getCode() == 200 ) {

                        ArrayList<ForoPost> listOfStrings = new ArrayList<ForoPost>(response.body().getResult().size());
                        listOfStrings.addAll(response.body().getResult());
                        posts = listOfStrings;
                        simpleProgressBar.setVisibility(View.GONE);
                        if(!status_call){
                            rv.setAdapter(new ForoPostAdapter(ForoPosts.this, posts, foro));
                        }else{
                            ArrayList<ForoPost> comentarios = new ArrayList<>();
                            for (ForoPost mainpost : posts) {
                                if (mainpost.getForoPostID() == idPost) {
                                    comentarios.addAll(mainpost.getPosts());
                                    break;
                                }
                            }
                            Intent intent = new Intent(getApplicationContext(), ListaPosts.class);
                            intent.putExtra("posts", (Serializable) comentarios);
                            intent.putExtra("foroInfo", (Serializable) foro);
                            intent.putExtra("comentario", comentario);
                            startActivity(intent);

                        }
                    }else{

                        simpleProgressBar.setVisibility(View.GONE);
                        new Toast(getApplicationContext()).setText("Error cargando foros disponibles");
                        Intent intent = new Intent(getApplicationContext(), Menu.class);
                        startActivity(intent);

                    }
                } catch (Exception e) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(ForoPosts.this,
                            "No se pudo cargar los foros",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<GenericResponse<ForoPost>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

    /*@Override
    public void onMethodCallback(int idPost) {
        reloadComments(false, 0);
    }*/

    @Override
    public void onMethodCallback(boolean status_call, int idPost, String comentario) {
        reloadComments(status_call, idPost, comentario);
    }

    @Override
    public int onCountVerComentariosCallback(int idPost) {
        ArrayList<ForoPost> comentarios = new ArrayList<>();
        for (ForoPost mainpost : posts) {
            if (mainpost.getForoPostID() == idPost) {
                comentarios.addAll(mainpost.getPosts());
                break;
            }
        }
        return comentarios.size();
    }

    @Override
    public void onClickVerComentariosCallback(int idPost, String comentario) {
        ArrayList<ForoPost> comentarios = new ArrayList<>();
        for (ForoPost mainpost : posts) {
            if (mainpost.getForoPostID() == idPost) {
                comentarios.addAll(mainpost.getPosts());
                break;
            }
        }

        Intent intent = new Intent(getApplicationContext(), ListaPosts.class);
        intent.putExtra("posts", (Serializable) comentarios);
        intent.putExtra("foroInfo", (Serializable) foro);
        intent.putExtra("comentario", comentario);
        startActivity(intent);

    }

}
