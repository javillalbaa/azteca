package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.ResponseComComentarioCarro;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;
import com.app.azteca.azteca.Controladores.presenters.responseCompartirCarro;
import com.app.azteca.azteca.Controladores.presenters.responseTemas;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CompartirCarroService {

    @GET("CompartirCarro")
    Call<GenericResponse<responseCompartirCarro>> getDatos(@Header("Authorization") String token);

    @GET("CompartirCarro/GetPosts")
    Call<GenericResponse<ResponseComComentarioCarro>> getComentarios(@Header("Authorization") String token, @Query("id") Integer id);

    @POST("CompartirCarro/CreatePost")
    Call<GenericResponse<Boolean>> create(@Header("Authorization") String token , @Body ResponseComComentarioCarro comentario );

    @POST("CompartirCarro/EstadoComentario")
    Call<GenericResponse<Boolean>> delete(@Header("Authorization") String token , @Body ResponseComComentarioCarro comentario );


}
