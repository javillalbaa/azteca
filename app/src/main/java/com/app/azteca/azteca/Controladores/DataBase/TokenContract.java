package com.app.azteca.azteca.Controladores.DataBase;

import android.provider.BaseColumns;

public class TokenContract {

    public static abstract class LawyerEntry implements BaseColumns {
        public static final String TABLE_NAME ="TokenStatus";

        public static final String ID = "id";
        public static final String TOKEN = "token";
        public static final String ID_USER = "ID_USER";
    }
}