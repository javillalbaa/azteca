package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.DataBase.TokenContract;
import com.app.azteca.azteca.Controladores.DataBase.TokenDbHelper;
import com.app.azteca.azteca.Controladores.adapter.MenuAdapter;
import com.app.azteca.azteca.Controladores.presenters.MenuResult;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.services.MenuService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Field;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Menu extends AppCompatActivity implements View.OnClickListener {

    private GridView gridView;
    private MenuAdapter adaptador;
    private List<MenuResult> menu;
    TokenDbHelper database;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu Azteca");
        setSupportActionBar(toolbar);

        FirebaseMessaging.getInstance().
                subscribeToTopic("AztecaCMS");

        gridView = (GridView) findViewById(R.id.grid);
        getMenu();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                MenuResult item = (MenuResult) parent.getItemAtPosition(position);
                try {
                    redirectToView(item);
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu_sub, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);

                                //Abrimos la base de datos 'DBUsuarios' en modo escritura
                                database = new TokenDbHelper(Menu.this);
                                sqLiteDatabase = database.getWritableDatabase();

                                String selectQuery = "DELETE FROM " + TokenContract.LawyerEntry.TABLE_NAME;
                                Cursor cursor = database.getReadableDatabase().rawQuery(selectQuery, null);

                                Intent intent = new Intent(Menu.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(Menu.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToView(MenuResult menuItem) throws NoSuchFieldException, IllegalAccessException {

        switch (menuItem.getNombre()) {
            case "Foro Azteca":
                foroMenuOption();
                break;
            case "Eventos":
                eventosMenuOption();
                break;
            case "Comparte tu Carro":
                CompartirCarroMenuOption();
                break;
            case "Buscar":
                BuscarContenidoMenuOption();
                break;
            case "Usuarios":
                BuscarUsuariosMenuOption();
                break;
            case "Galeria":
                galeria();
                break;
            default:
                defaultMenuOption(menuItem);
        }
    }

    private void galeria() {

        try {
            Intent intent = new Intent(getApplicationContext(), Galeria.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }

    private void foroMenuOption() {

        try {
            Intent intent = new Intent(getApplicationContext(), ListaForos.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }

    private void CompartirCarroMenuOption() {

        try {
            Intent intent = new Intent(getApplicationContext(), CompartirCarro.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }
    private void BuscarUsuariosMenuOption() {

        try {
            Intent intent = new Intent(getApplicationContext(), BuscarUsuarios.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }
    private void BuscarContenidoMenuOption() {

        try {
            Intent intent = new Intent(getApplicationContext(), BuscarContenido.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }
    private void eventosMenuOption() {

        try {
            Intent intent = new Intent(getApplicationContext(), Eventos.class);
            startActivity(intent);
        } catch (Exception ex) {
            Exception gg = ex;
        }
    }

    private void defaultMenuOption(MenuResult menuItem) throws NoSuchFieldException, IllegalAccessException {
        Intent intent = new Intent(getApplicationContext(), subMenu.class);
        Field field = constant.class.getDeclaredField("subcategoria");
        field.setAccessible(true);
        field.set(null, menuItem.getSubcategorias());
        startActivity(intent);
    }


    private void getMenu() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
        MenuService service = retrofit.create(MenuService.class);

        Call<GenericResponse<MenuResult>> call = service.getMenu(constant.getToken());

        call.enqueue(new Callback<GenericResponse<MenuResult>>() {

            @Override
            public void onResponse(Call<GenericResponse<MenuResult>> call, Response<GenericResponse<MenuResult>> response) {
                //gridView = (GridView) findViewById(R.id.grid);
                menu = response.body().getResult();
                menu.remove(0);
                adaptador = new MenuAdapter(getApplicationContext(), menu);
                gridView.setAdapter(adaptador);
            }

            @Override
            public void onFailure(Call<GenericResponse<MenuResult>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }


    @Override
    public void onClick(View view) {
    }

}
