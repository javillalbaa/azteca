package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import com.app.azteca.azteca.Controladores.adapter.GaleriaAdapter;
import com.app.azteca.azteca.Controladores.presenters.Item;
import com.app.azteca.azteca.Controladores.presenters.ResponseGaleria;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.services.TemasService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GaleriaTema extends AppCompatActivity implements SimpleGestureFilter.SimpleGestureListener {

    private GridView gridView;
    private GaleriaAdapter adaptador;
    private int pagina = 1;
    private List<Item> imagenes;
    private SimpleGestureFilter detector;
    private int tema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria_tema);

        tema = getIntent().getExtras().getInt("tema");
        String titulo = getIntent().getExtras().getString("tit");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(titulo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridView = (GridView) findViewById(R.id.grid);
        getMenu();

        detector = new SimpleGestureFilter(this,this);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Item item = (Item) parent.getItemAtPosition(position);
                redirectToView(item);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(GaleriaTema.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(GaleriaTema.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToView(Item item){
        Intent intent = new Intent(getApplicationContext(), ImgGaleria.class);
        intent.putExtra("img", item.getArchivo());
        intent.putExtra("desc", item.getDescripcion());
        startActivity(intent);
    }

    private void getMenu() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
        TemasService service = retrofit.create(TemasService.class);

        Call<ResponseGaleria> call = service.getImages(constant.getToken(), tema, pagina, 6);

        call.enqueue(new Callback<ResponseGaleria>() {

            @Override
            public void onResponse(Call<ResponseGaleria> call, Response<ResponseGaleria> response) {

                imagenes = response.body().getResultPag().getItems();
                String paginador = "Pagina " + pagina + " de " + Integer.toString(response.body().getResultPag().getPaginasTotales());
                if(imagenes.size() > 0){
                    adaptador = new GaleriaAdapter(getApplicationContext(), imagenes);
                    gridView.setAdapter(adaptador);
                    Toast.makeText(GaleriaTema.this,
                            paginador,
                            Toast.LENGTH_SHORT).show();
                }else{
                    pagina--;
                    Toast.makeText(GaleriaTema.this,
                            "No hay más imagenes para ver",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGaleria> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

    public void Volver(View view) {
        if(pagina >= 2){
            pagina--;
            getMenu();
        }else{
            Toast.makeText(GaleriaTema.this,
                    "No hay más imagenes para ver",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void avanzar(View view) {
        pagina++;
        getMenu();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT :
                if(pagina >= 2){
                    pagina--;
                    getMenu();
                }else{
                    Toast.makeText(GaleriaTema.this,
                            "No hay más imagenes para ver",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case SimpleGestureFilter.SWIPE_LEFT :
                pagina++;
                getMenu();
                break;
        }
    }

    @Override
    public void onDoubleTap() {
        //Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

}
