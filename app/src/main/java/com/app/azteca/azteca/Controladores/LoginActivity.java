package com.app.azteca.azteca.Controladores;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.app.azteca.azteca.Controladores.DataBase.TokenContract;
import com.app.azteca.azteca.Controladores.DataBase.TokenDbHelper;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseLogin;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Field;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    //private Peticion mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    TokenDbHelper database;
    SQLiteDatabase sqLiteDatabase;
    private ProgressBar simpleProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //settingsLoading = new SettingsLoading(this,R.id.loading);
        setContentView(R.layout.activity_login);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        //populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);

        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        /*database = new TokenDbHelper(LoginActivity.this);
        sqLiteDatabase = database.getWritableDatabase();

        //database.getReadableDatabase().delete("TokenStatus");
        String selectQuery = "DELETE FROM " + TokenContract.LawyerEntry.TABLE_NAME;
        Cursor cursor = database.getReadableDatabase().rawQuery(selectQuery, null);*/


        //sqLiteDatabase.close();

        Cursor c = sqLiteDatabase.rawQuery("select count(*) as count_ from " + TokenContract.LawyerEntry.TABLE_NAME, null);

        while(c.moveToNext()){
            String name = c.getString(c.getColumnIndex("count_"));
            Log.d("Datos :", name );
        }


        if(!constant.getToken().equals("")){
            Intent intent = new Intent(LoginActivity.this, Menu.class);
            startActivity(intent);
        }


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // validacion de usuario.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("El usuario es requerido");
            focusView = mEmailView;
            cancel = true;
        }

        if(TextUtils.isEmpty(password)){
            mPasswordView.setError("la contraseña es requerida");
            focusView = mPasswordView;
            cancel = true;
        }


        if (cancel) {
            //Nos lleva al campo donde la validación ha fallado
            focusView.requestFocus();
        } else {
            simpleProgressBar.setVisibility(View.VISIBLE);
            String email2 = "exdfonseca", password2="Temporal123*2018";
            loguear(email2, password2);
        }
    }

    private void loguear(String user, String pass) {

        final String User = user;
        final String Pass = pass;
        final Context context= this;

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        LoginService serviceslogin = retrofit.create(LoginService.class);

        Call<TokenResponse> call = serviceslogin.getToken(new responseLogin(User, Pass));
        //Call<TokenResponse> call = serviceTuto.body(new LoginResponse("javier","s"));

        call.enqueue(new Callback<TokenResponse>() {

            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                try {
                    if( response.body().getMessage().getCode() == 200 ) {
                        Field field = constant.class.getDeclaredField("Token");
                        field.setAccessible(true);
                        field.set(null, response.body().getResult().get(0).getToken());
                        Field field_id = constant.class.getDeclaredField("idUser");
                        field_id.setAccessible(true);
                        field_id.set(null, Integer.parseInt(response.body().getResult().get(0).getUsuarioid()));

                        if (!constant.getToken().isEmpty()) {

                            //Abrimos la base de datos 'DBUsuarios' en modo escritura
                            /*database = new TokenDbHelper(LoginActivity.this);
                            sqLiteDatabase = database.getWritableDatabase();*/

                            /*ContentValues values = new ContentValues();
                            values.put(TokenContract.LawyerEntry.TOKEN, constant.getToken());
                            values.put(TokenContract.LawyerEntry.ID_USER, constant.getIdUser());

                            sqLiteDatabase.insert(
                                    TokenContract.LawyerEntry.TABLE_NAME,
                                    null,
                                    values
                            );*/

                            simpleProgressBar.setVisibility(View.GONE);
                            Intent intent = new Intent(getApplicationContext(), Menu.class);
                            startActivity(intent);
                        } else {
                            simpleProgressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this,
                                    "Error en el servidor",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }else{
                        simpleProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this,
                                response.body().getMessage().getMessage(),
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    System.out.println("No se pudo cambiar el valor :(");
                    e.printStackTrace(System.out);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}

