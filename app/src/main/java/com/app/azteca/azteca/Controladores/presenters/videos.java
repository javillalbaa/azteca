package com.app.azteca.azteca.Controladores.presenters;

public class videos {
    private int video;
    private String url;
    private String nombre;

    public videos(int video, String url, String nombre) {
        this.video = video;
        this.url = url;
        this.nombre = nombre;
    }

    public int getVideo() {
        return video;
    }

    public void setVideo(int video) {
        this.video = video;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
