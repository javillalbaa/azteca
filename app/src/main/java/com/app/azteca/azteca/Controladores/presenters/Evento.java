package com.app.azteca.azteca.Controladores.presenters;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Evento implements Serializable
{

    @SerializedName("EventoID")
    @Expose
    private Integer eventoID;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("FechaEvento")
    @Expose
    private String fechaEvento;
    @SerializedName("Hora")
    @Expose
    private String hora;
    @SerializedName("CategoriaEventoID")
    @Expose
    private Integer categoriaEventoID;
    @SerializedName("CategoriaNombre")
    @Expose
    private String categoriaNombre;
    @SerializedName("Color")
    @Expose
    private String color;
    @SerializedName("Lugar")
    @Expose
    private String lugar;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("FileID")
    @Expose
    private Integer fileID;
    @SerializedName("FilePath")
    @Expose
    private String filePath;
    @SerializedName("Archivo")
    @Expose
    private String archivo;
    @SerializedName("ContentType")
    @Expose
    private String contentType;
    @SerializedName("Activo")
    @Expose
    private Boolean activo;
    @SerializedName("FechaInsercion")
    @Expose
    private String fechaInsercion;
    @SerializedName("FechaModificacion")
    @Expose
    private String fechaModificacion;

    /**
     * No args constructor for use in serialization
     *
     */
    public Evento() {
    }

    /**
     *
     * @param categoriaNombre
     * @param filePath
     * @param descripcion
     * @param contentType
     * @param eventoID
     * @param activo
     * @param fechaInsercion
     * @param fechaEvento
     * @param nombre
     * @param hora
     * @param fileID
     * @param color
     * @param archivo
     * @param lugar
     * @param fechaModificacion
     * @param categoriaEventoID
     */
    public Evento(Integer eventoID, String nombre, String fechaEvento, String hora, Integer categoriaEventoID, String categoriaNombre, String color, String lugar, String descripcion, Integer fileID, String filePath, String archivo, String contentType, Boolean activo, String fechaInsercion, String fechaModificacion) {
        super();
        this.eventoID = eventoID;
        this.nombre = nombre;
        this.fechaEvento = fechaEvento;
        this.hora = hora;
        this.categoriaEventoID = categoriaEventoID;
        this.categoriaNombre = categoriaNombre;
        this.color = color;
        this.lugar = lugar;
        this.descripcion = descripcion;
        this.fileID = fileID;
        this.filePath = filePath;
        this.archivo = archivo;
        this.contentType = contentType;
        this.activo = activo;
        this.fechaInsercion = fechaInsercion;
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getEventoID() {
        return eventoID;
    }

    public void setEventoID(Integer eventoID) {
        this.eventoID = eventoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(String fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Integer getCategoriaEventoID() {
        return categoriaEventoID;
    }

    public void setCategoriaEventoID(Integer categoriaEventoID) {
        this.categoriaEventoID = categoriaEventoID;
    }

    public String getCategoriaNombre() {
        return categoriaNombre;
    }

    public void setCategoriaNombre(String categoriaNombre) {
        this.categoriaNombre = categoriaNombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getFileID() {
        return fileID;
    }

    public void setFileID(Integer fileID) {
        this.fileID = fileID;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

}
