package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.Contenido;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.Usuario;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface BuscarContenidosService {

    @GET("Buscador")
    Call<GenericResponse<Contenido>> getContenidos(@Header("Authorization") String token, @Query("ParametroBusqueda") String parametroBusqueda);

}
