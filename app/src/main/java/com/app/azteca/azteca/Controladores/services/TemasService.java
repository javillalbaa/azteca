package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.MenuResult;
import com.app.azteca.azteca.Controladores.presenters.ResponseGaleria;
import com.app.azteca.azteca.Controladores.presenters.ResultPag;
import com.app.azteca.azteca.Controladores.presenters.responseLogin;
import com.app.azteca.azteca.Controladores.presenters.responseTemas;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface TemasService {

    @GET("Galeria/GetTemas")
    Call<GenericResponse<responseTemas>> getTemas(@Header("Authorization") String token);

    @GET("Galeria")
    Call<ResponseGaleria> getImages(
            @Header("Authorization") String token,
            @Query("IdTema") Integer IdTema,
            @Query("pagina") Integer pagina,
            @Query("itemsPagina") Integer itemsPagina
    );

}
