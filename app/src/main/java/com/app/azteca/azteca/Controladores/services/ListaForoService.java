package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ListaForoService {

    @GET("Foro")
    Call<GenericResponse<Foro>> getData(@Header("Authorization") String token);

}
