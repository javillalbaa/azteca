package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseComComentarioCarro implements Serializable {

    @SerializedName("CompartirCarroPostID")
    @Expose
    private int compartirCarroPostID;
    @SerializedName("CompartirCarroID")
    @Expose
    private int compartirCarroID;
    @SerializedName("Post")
    @Expose
    private String post;
    @SerializedName("Activo")
    @Expose
    private boolean activo;
    @SerializedName("UsuarioID")
    @Expose
    private int usuarioID;
    @SerializedName("NombresCompletos")
    @Expose
    private String nombresCompletos;
    @SerializedName("CorreoElectronico")
    @Expose
    private String correoElectronico;
    @SerializedName("FechaInsercion")
    @Expose
    private String fechaInsercion;
    private int Accion;
    private int PostId;

    public ResponseComComentarioCarro(int compartirCarroPostID, int compartirCarroID, String post, boolean activo, int usuarioID, String nombresCompletos, String correoElectronico, String fechaInsercion, int accion, int PostId) {
        this.compartirCarroPostID = compartirCarroPostID;
        this.compartirCarroID = compartirCarroID;
        this.post = post;
        this.activo = activo;
        this.usuarioID = usuarioID;
        this.nombresCompletos = nombresCompletos;
        this.correoElectronico = correoElectronico;
        this.fechaInsercion = fechaInsercion;
        Accion = accion;
        this.PostId = PostId;
    }

    public int getPostId() {
        return PostId;
    }

    public void setPostId(int postId) {
        PostId = postId;
    }

    public int getAccion() {
        return Accion;
    }

    public void setAccion(int accion) {
        Accion = accion;
    }

    public ResponseComComentarioCarro() {
    }

    public int getCompartirCarroPostID() {
        return compartirCarroPostID;
    }

    public void setCompartirCarroPostID(int compartirCarroPostID) {
        this.compartirCarroPostID = compartirCarroPostID;
    }

    public int getCompartirCarroID() {
        return compartirCarroID;
    }

    public void setCompartirCarroID(int compartirCarroID) {
        this.compartirCarroID = compartirCarroID;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(int usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNombresCompletos() {
        return nombresCompletos;
    }

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

}
