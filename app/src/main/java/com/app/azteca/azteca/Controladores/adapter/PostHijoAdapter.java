package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.R;

import java.util.List;

public class PostHijoAdapter extends BaseAdapter {

    private Context context;
    private List<ForoPost> posts;

    public PostHijoAdapter(Context context, List<ForoPost> posts) {
        this.context = context;
        this.posts = posts;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public ForoPost getItem(int i) {
        return posts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getForoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.post_hijo, viewGroup, false);
        }

        TextView comentario = (TextView) view.findViewById(R.id.txv_subpost);
        TextView autor = (TextView) view.findViewById(R.id.txv_postHijoAutor);
        //TextView fecha = (TextView) view.findViewById(R.id.txv_postHijoFecha);

        final ForoPost item = getItem(i);

        comentario.setText(item.getPost());
        autor.setText(item.getNombresCompletos());
        //fecha.setText(item.getFechaPost().toString());

        return view;
    }
}