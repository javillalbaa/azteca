package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    //token
    @SerializedName("TokenStatus")
    @Expose
    private String token;

    //menu
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("Icono")
    @Expose
    private String icono;
    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Subcategorias")
    @Expose
    private List<Subcategoria> subcategorias = null;

    //Categorias
    @SerializedName("ContenidoID")
    @Expose
    private int contenidoID;
    @SerializedName("CategoriaContenidoID")
    @Expose
    private int categoriaContenidoID;
    @SerializedName("CategoriaContenido")
    @Expose
    private String categoriaContenido;
    @SerializedName("Titulo")
    @Expose
    private String titulo;
    @SerializedName("SubTitulo")
    @Expose
    private String subTitulo;
    @SerializedName("DescripcionCorta")
    @Expose
    private String descripcionCorta;
    @SerializedName("DescripcionLarga")
    @Expose
    private String descripcionLarga;
    @SerializedName("Archivo")
    @Expose
    private String archivo;
    @SerializedName("ContentType")
    @Expose
    private String contentType;
    @SerializedName("FechaInsercion")
    @Expose
    private String fechaInsercion;
    @SerializedName("FechaModificacion")
    @Expose
    private String fechaModificacion;


    /**
     * No args constructor for use in serialization
     */
    public Result() {
    }

    /**
     * @param token
     */
    public Result(String token) {
        super();
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @param id
     * @param nombre
     * @param subcategorias
     * @param icono
     */
    public Result(String nombre, String icono, int id, List<Subcategoria> subcategorias) {
        super();
        this.nombre = nombre;
        this.icono = icono;
        this.id = id;
        this.subcategorias = subcategorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Subcategoria> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(List<Subcategoria> subcategorias) {
        this.subcategorias = subcategorias;
    }


    /**
     * @param contenidoID
     * @param descripcionLarga
     * @param subTitulo
     * @param descripcionCorta
     * @param contentType
     * @param fechaInsercion
     * @param categoriaContenidoID
     * @param titulo
     * @param archivo
     * @param fechaModificacion
     * @param categoriaContenido
     */
    public Result(int contenidoID, int categoriaContenidoID, String categoriaContenido, String titulo, String subTitulo, String descripcionCorta, String descripcionLarga, String archivo, String contentType, String fechaInsercion, String fechaModificacion) {
        super();
        this.contenidoID = contenidoID;
        this.categoriaContenidoID = categoriaContenidoID;
        this.categoriaContenido = categoriaContenido;
        this.titulo = titulo;
        this.subTitulo = subTitulo;
        this.descripcionCorta = descripcionCorta;
        this.descripcionLarga = descripcionLarga;
        this.archivo = archivo;
        this.contentType = contentType;
        this.fechaInsercion = fechaInsercion;
        this.fechaModificacion = fechaModificacion;
    }

    public int getContenidoID() {
        return contenidoID;
    }

    public void setContenidoID(int contenidoID) {
        this.contenidoID = contenidoID;
    }

    public int getCategoriaContenidoID() {
        return categoriaContenidoID;
    }

    public void setCategoriaContenidoID(int categoriaContenidoID) {
        this.categoriaContenidoID = categoriaContenidoID;
    }

    public String getCategoriaContenido() {
        return categoriaContenido;
    }

    public void setCategoriaContenido(String categoriaContenido) {
        this.categoriaContenido = categoriaContenido;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubTitulo() {
        return subTitulo;
    }

    public void setSubTitulo(String subTitulo) {
        this.subTitulo = subTitulo;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public void setDescripcionCorta(String descripcionCorta) {
        this.descripcionCorta = descripcionCorta;
    }

    public String getDescripcionLarga() {
        return descripcionLarga;
    }

    public void setDescripcionLarga(String descripcionLarga) {
        this.descripcionLarga = descripcionLarga;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

}
