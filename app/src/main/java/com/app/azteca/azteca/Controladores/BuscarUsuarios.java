package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.ListaUsuariosAdapter;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.Usuario;
import com.app.azteca.azteca.Controladores.services.BuscarUsuariosService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuscarUsuarios extends AppCompatActivity {
    TextView textView;
    EditText editText;
    ListView listView;
    ProgressBar simpleProgressBar;
    TextView textViewnoResults;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_usuarios);
        textView=findViewById(R.id.textviewbuscarusuarios);
        textViewnoResults=findViewById(R.id.textviewNoResults);
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbarBuscarUsuarios);
        toolbar.setTitle("Usuarios");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        listView = findViewById(R.id.listviewBuscarUsuarios);

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(BuscarUsuarios.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(BuscarUsuarios.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getUsuarios(View view){
        editText = findViewById(R.id.edittextparametrobusqueda);
        textViewnoResults.setVisibility(View.GONE);
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        BuscarUsuariosService buscarUsuariosService = retrofit.create(BuscarUsuariosService.class);
        Call<GenericResponse<Usuario>> call = buscarUsuariosService.getUsuarios(constant.getToken(),editText.getText().toString());
        //Call<TokenResponse> call = serviceTuto.body(new LoginResponse("javier","s"));

        call.enqueue(new Callback<GenericResponse<Usuario>>() {

            @Override
            public void onResponse(Call<GenericResponse<Usuario>> call, Response<GenericResponse<Usuario>> response) {
                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        Log.d("Datos:",response.body().toString());
                        if(response.body().getResult().size()==0){
                            textViewnoResults.setVisibility(View.VISIBLE);
                        }else {
                            List<Usuario> usuarios = response.body().getResult();
                            ListaUsuariosAdapter adapter = new ListaUsuariosAdapter(getApplicationContext(), usuarios);
                            listView.setAdapter(adapter);
                        }
                    }else{
                        new Toast(getApplicationContext()).setText("Error cargando usuarios");
                    }
                }catch (Exception e){
                    simpleProgressBar.setVisibility(View.GONE);
                    Toast.makeText(BuscarUsuarios.this,
                            "No se pudo cargar las usuarios",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Usuario>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }

        });
    }


}
