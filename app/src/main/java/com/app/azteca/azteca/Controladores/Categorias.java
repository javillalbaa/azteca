package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.CategoriaAdapter;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.services.CategoriaService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Categorias extends AppCompatActivity {

    //private int categoriaId;
    private ListView listview;
    private CategoriaAdapter categoria;
    private ArrayList<responseCategoria> cont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Volver");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listview = (ListView) findViewById(R.id.listview);
        getCategorias();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                responseCategoria item = (responseCategoria) parent.getItemAtPosition(position);
                redirectToView(item);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(Categorias.this, com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(Categorias.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(Categorias.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToView(responseCategoria infCategosria){
        Log.e("Funciona", infCategosria.toString());
        Intent intent = new Intent(Categorias.this, InfoCategoria.class);
        intent.putExtra("inf_categoria", (Serializable)infCategosria);
        startActivity(intent);
    }

    private void getCategorias(){

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CategoriaService servicescategoria = retrofit.create(CategoriaService.class);
        Call<GenericResponse<responseCategoria>> call = servicescategoria.getCategorias(constant.getToken(), constant.getIdCategoria());

        call.enqueue(new Callback<GenericResponse<responseCategoria>>() {

            @Override
            public void onResponse(Call<GenericResponse<responseCategoria>> call, Response<GenericResponse<responseCategoria>> response) {
                ArrayList<responseCategoria> listOfStrings = new ArrayList<responseCategoria>(response.body().getResult().size());
                listOfStrings.addAll(response.body().getResult());
                cont = listOfStrings;
                categoria = new CategoriaAdapter(Categorias.this, cont);
                listview.setAdapter(categoria);
            }

            @Override
            public void onFailure(Call<GenericResponse<responseCategoria>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

}
