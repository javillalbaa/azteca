package com.app.azteca.azteca.Controladores.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.azteca.azteca.R;


public class ForoPostViewHolder extends RecyclerView.ViewHolder {

    TextView nametxt;
    TextView txtIdPost;
    TextView autortxt;
    TextView fechatxt;
    TextView buttonViewOption;
    Button btnComentar;
    Button btnComentarPost;
    EditText commetField;
    RelativeLayout commentField;
    ListView listview;
    Button btnFrComentarios;

    public ForoPostViewHolder(View itemView) {
        super(itemView);
        nametxt = (TextView) itemView.findViewById(R.id.nameTxt);
        autortxt = (TextView) itemView.findViewById(R.id.txv_autor);
        txtIdPost = (TextView) itemView.findViewById(R.id.txv_idpost);
        buttonViewOption = (TextView) itemView.findViewById(R.id.textViewOptions);

    }
}