package com.app.azteca.azteca.Controladores.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.LoginActivity;
import com.app.azteca.azteca.Controladores.Menu;
import com.app.azteca.azteca.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import retrofit2.http.Url;

public class AztecaFirebaseMessagingService extends FirebaseMessagingService{

    private static final String TAG="FirebaseMessagingServic";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String from = remoteMessage.getFrom();
        Log.d(TAG, "Mensaje recibido de " + from);

        if(remoteMessage.getNotification() != null){
            Log.d(TAG, "Notificacion" + remoteMessage.getNotification().getBody());
        }

        if(remoteMessage.getData().size() > 0){

            String strTitle = remoteMessage.getData().get("title").toString();//remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getData().get("text").toString();
            MostrarNotificacion(strTitle, message);

        }


    }

    private void MostrarNotificacion(String tit, String msg) {

        Intent intent = new Intent(this, Menu.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder b = new NotificationCompat.Builder(this);

        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.logo_blanco)
                .setContentTitle(tit)
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent);


        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, b.build());


    }
}
