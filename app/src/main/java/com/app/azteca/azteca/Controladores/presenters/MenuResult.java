package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MenuResult {

    @SerializedName("Token")
    @Expose
    private String token;

    @SerializedName("UsuarioId")
    @Expose
    private String usuarioid;

    @SerializedName("Nombre")
    @Expose
    private String nombre;

    @SerializedName("Icono")
    @Expose
    private String icono;

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Subcategorias")
    @Expose
    private List<Subcategoria> subcategorias = null;


    /**
     * No args constructor for use in serialization
     */
    public MenuResult() {
    }

    /**
     * @param token
     */
    public MenuResult(String token, String usuarioid) {
        super();
        this.token = token;
        this.usuarioid = usuarioid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(String usuarioid) {
        this.usuarioid = usuarioid;
    }

    /**
     *
     * @param id
     * @param nombre
     * @param subcategorias
     * @param icono
     */
    public MenuResult(String nombre, String icono, int id, List<Subcategoria> subcategorias) {
        super();
        this.nombre = nombre;
        this.icono = icono;
        this.id = id;
        this.subcategorias = subcategorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Subcategoria> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(List<Subcategoria> subcategorias) {
        this.subcategorias = subcategorias;
    }

}
