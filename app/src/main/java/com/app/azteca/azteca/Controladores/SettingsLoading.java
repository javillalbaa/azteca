package com.app.azteca.azteca.Controladores;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.app.azteca.azteca.R;

public class SettingsLoading {
    AppCompatActivity appCompatActivity;
    int id;
    FragmentManager fm;
    FragmentTransaction ft;
    public SettingsLoading(AppCompatActivity appCompatActivity,int id){
        this.appCompatActivity=appCompatActivity;
        this.id=id;
    }
    public void show(){
        this.fm = this.appCompatActivity.getSupportFragmentManager();
        this.ft = this.fm.beginTransaction();
        this.ft.add(new Loading(), "algo");
        this.ft.replace(this.id, new Loading());
        this.ft.commit();
    }
    public void hide(){
        this.fm = this.appCompatActivity.getSupportFragmentManager();
        this.ft = this.fm.beginTransaction();
        this.ft.add(new Loading(), "algo");
        this.ft.replace(this.id, new BlankFragment());
        this.ft.commit();
    }
}
