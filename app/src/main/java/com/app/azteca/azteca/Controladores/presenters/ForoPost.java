package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForoPost implements Serializable
{

    @SerializedName("ForoPostID")
    @Expose
    private Integer foroPostID;
    @SerializedName("ForoPostPadre")
    @Expose
    private Integer foroPostPadre;
    @SerializedName("ForoID")
    @Expose
    private Integer foroID;
    @SerializedName("Tema")
    @Expose
    private String tema;
    @SerializedName("Post")
    @Expose
    private String post;
    @SerializedName("CalificacionLike")
    @Expose
    private Integer calificacionLike;
    @SerializedName("CalificacionDislike")
    @Expose
    private Integer calificacionDislike;
    @SerializedName("Verificado")
    @Expose
    private Boolean verificado;
    @SerializedName("Activo")
    @Expose
    private Boolean activo;
    @SerializedName("UsuarioID")
    @Expose
    private Integer usuarioID;
    @SerializedName("NombresCompletos")
    @Expose
    private String nombresCompletos;
    @SerializedName("CorreoElectronico")
    @Expose
    private String correoElectronico;
    @SerializedName("FechaPost")
    @Expose
    private String fechaPost;
    private final static long serialVersionUID = -8611876777971775819L;

    @SerializedName("Posts")
    @Expose
    private List<ForoPost> posts = null;


    /**
     * No args constructor for use in serialization
     *
     */
    public ForoPost() {
    }

    /**
     *
     * @param post
     * @param fechaPost
     * @param verificado
     * @param tema
     * @param activo
     * @param usuarioID
     * @param foroPostID
     * @param correoElectronico
     * @param foroID
     * @param calificacionDislike
     * @param nombresCompletos
     * @param calificacionLike
     * @param foroPostPadre
     */
    public ForoPost(Integer foroPostID, Integer foroPostPadre, Integer foroID, String tema, String post, Integer calificacionLike, Integer calificacionDislike, Boolean verificado, Boolean activo, Integer usuarioID, String nombresCompletos, String correoElectronico, String fechaPost, List<ForoPost> posts ) {
        super();
        this.foroPostID = foroPostID;
        this.foroPostPadre = foroPostPadre;
        this.foroID = foroID;
        this.tema = tema;
        this.post = post;
        this.calificacionLike = calificacionLike;
        this.calificacionDislike = calificacionDislike;
        this.verificado = verificado;
        this.activo = activo;
        this.usuarioID = usuarioID;
        this.nombresCompletos = nombresCompletos;
        this.correoElectronico = correoElectronico;
        this.fechaPost = fechaPost;
        this.posts = posts;
    }

    public Integer getForoPostID() {
        return foroPostID;
    }

    public void setForoPostID(Integer foroPostID) {
        this.foroPostID = foroPostID;
    }

    public Integer getForoPostPadre() {
        return foroPostPadre;
    }

    public void setForoPostPadre(Integer foroPostPadre) {
        this.foroPostPadre = foroPostPadre;
    }

    public Integer getForoID() {
        return foroID;
    }

    public void setForoID(Integer foroID) {
        this.foroID = foroID;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Integer getCalificacionLike() {
        return calificacionLike;
    }

    public void setCalificacionLike(Integer calificacionLike) {
        this.calificacionLike = calificacionLike;
    }

    public Integer getCalificacionDislike() {
        return calificacionDislike;
    }

    public void setCalificacionDislike(Integer calificacionDislike) {
        this.calificacionDislike = calificacionDislike;
    }

    public Boolean getVerificado() {
        return verificado;
    }

    public void setVerificado(Boolean verificado) {
        this.verificado = verificado;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNombresCompletos() {
        return nombresCompletos;
    }

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaPost() {
        return fechaPost;
    }

    public void setFechaPost(String fechaPost) {
        this.fechaPost = fechaPost;
    }

    public List<ForoPost> getPosts() {
        return this.posts;
    }

    public void setPosts(List<ForoPost> posts) {
        this.posts = posts;
    }

}