package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseGaleria {

    @SerializedName("ResultPag")
    @Expose
    private ResultPag resultPag;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseGaleria() {
    }

    /**
     *
     * @param resultPag
     */
    public ResponseGaleria(ResultPag resultPag) {
        super();
        this.resultPag = resultPag;
    }

    public ResultPag getResultPag() {
        return resultPag;
    }

    public void setResultPag(ResultPag resultPag) {
        this.resultPag = resultPag;
    }
}
