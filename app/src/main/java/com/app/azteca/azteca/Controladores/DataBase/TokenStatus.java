package com.app.azteca.azteca.Controladores.DataBase;

public class TokenStatus {

    private String token;
    private int id_user;

    public TokenStatus(String token, int id_user) {
        this.token = token;
        this.id_user = id_user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

}
