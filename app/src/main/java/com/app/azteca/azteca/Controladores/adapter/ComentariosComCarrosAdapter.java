package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.InfCompartirCarro;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.ResponseComComentarioCarro;
import com.app.azteca.azteca.Controladores.services.CompartirCarroService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ComentariosComCarrosAdapter extends RecyclerView.Adapter<ForoPostViewHolder> {

    private Context context;
    private List<ResponseComComentarioCarro> comentariocarro;
    RecyclerView rv;
    Retrofit retrofit;
    Foro foro;
    private ViewGroup row;
    private int PostID;
    private AdapterCompartirCarroPostCallback adapertCallBack;

    public ComentariosComCarrosAdapter(Context context, List<ResponseComComentarioCarro> comentariocarro) {
        this.context = context;
        this.comentariocarro = comentariocarro;
        this.adapertCallBack = ((AdapterCompartirCarroPostCallback) context);
        this.retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }


    @NonNull
    @Override
    public ForoPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_comentario_carro, parent, false);
        return new ForoPostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ForoPostViewHolder holder, final int position) {

        holder.nametxt.setText(comentariocarro.get(position).getPost());
        holder.autortxt.setText(comentariocarro.get(position).getNombresCompletos());
        holder.txtIdPost.setText(Integer.toString(comentariocarro.get(position).getCompartirCarroPostID()));

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                row = (ViewGroup) v.getParent().getParent();
                PostID = Integer.parseInt(((TextView) row.findViewById(R.id.txv_idpost)).getText().toString());
                //creating a popup menu
                PopupMenu popup2 = new PopupMenu(context, holder.buttonViewOption);
                //inflating menu from xml resource
                popup2.inflate(R.menu.options_menu_carro);


                popup2.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:

                                ResponseComComentarioCarro borrar_comentario = new ResponseComComentarioCarro();
                                borrar_comentario.setPostId(PostID);
                                borrar_comentario.setUsuarioID(constant.getIdUser());
                                borrar_comentario.setAccion(3);

                                CompartirCarroService service = retrofit.create(CompartirCarroService.class);
                                Call<GenericResponse<Boolean>> call = service.delete(constant.getToken(), borrar_comentario);
                                call.enqueue(new Callback<GenericResponse<Boolean>>() {

                                    @Override
                                    public void onResponse(Call<GenericResponse<Boolean>> call, Response<GenericResponse<Boolean>> response) {

                                        try{
                                            if( response.body().getMessage().getCode() == 200 ) {
                                                Log.e("Funciona", response.body().toString());
                                                adapertCallBack.recargarComentarios();
                                                Toast.makeText(context,
                                                        "Mensaje eliminado",
                                                        Toast.LENGTH_SHORT).show();
                                            }else{
                                                //new Toast(getApplicationContext()).setText("Error cargando comentarío");
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(context,
                                                    "No se pudo cargar los comentario",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<GenericResponse<Boolean>> call, Throwable t) {
                                        Log.e("Error test:   ", t.toString());
                                    }
                                });
                                break;
                        }
                        return false;
                    }
                });

                popup2.show();

            }

        });
    }

    @Override
    public int getItemCount() {
        return comentariocarro.size();
    }

    public interface AdapterCompartirCarroPostCallback {
        void recargarComentarios();
    }
}
