package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.responseCompartirCarro;
import com.app.azteca.azteca.Controladores.presenters.responseTemas;
import com.app.azteca.azteca.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CompartirCarroAdapter extends BaseAdapter {

    private Context context;
    private List<responseCompartirCarro> com_carro;

    public CompartirCarroAdapter(Context context, List<responseCompartirCarro> com_carro) {
        this.context = context;
        this.com_carro = com_carro;
    }

    @Override
    public int getCount() {
        return com_carro.size();
    }

    @Override
    public responseCompartirCarro getItem(int i) {
        return com_carro.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getCompartirCarroID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_compartir_carro, viewGroup, false);
        }

        TextView informacion = (TextView) view.findViewById(R.id.informacion);
        final responseCompartirCarro item = getItem(i);
        String informacion_ = "<font color=#424242>Destino:</font><font color=#9e9e9e>"+ item.getDestino() + "</font><br>";
        informacion_ += "<font color=#424242>Nombre Contacto:</font><font color=#9e9e9e>"+ item.getNombreContacto() + "</font><br>";
        informacion_ += "<font color=#424242>Teléfono Contacto:</font><font color=#9e9e9e>"+ item.getTelefonoContacto() + "</font><br>";
        informacion_ += "<font color=#424242>Fecha y Hora de Salida:</font><font color=#9e9e9e>" + item.getFechaSalida().substring(0, 10) + " " + item.getHoraSalida() + "</font><br>";
        informacion_ += "<font color=#424242>Punto de Encuentro:</font><font color=#9e9e9e>" + item.getPuntoEncuentro() + "</font><br>";
        informacion_ += "<font color=#424242>Descripción de la Ruta:</font><font color=#9e9e9e>" + item.getDescripcion() + "</font>";
        informacion.setText(Html.fromHtml(informacion_));
        return view;
    }

}
