package com.app.azteca.azteca.Controladores.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TokenDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Lawyers.db";

    public TokenDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE " + TokenContract.LawyerEntry.TABLE_NAME + " ("
                + TokenContract.LawyerEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TokenContract.LawyerEntry.TOKEN + " TEXT,"
                + TokenContract.LawyerEntry.ID_USER + " INTEGER,"
                + "UNIQUE (" + TokenContract.LawyerEntry.ID + "))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
