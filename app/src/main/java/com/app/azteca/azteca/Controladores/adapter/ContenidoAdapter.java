package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.app.azteca.azteca.Controladores.presenters.ResponseContenido;
import com.app.azteca.azteca.R;

import java.util.List;

public class ContenidoAdapter extends BaseAdapter {

    private Context context;
    private List<ResponseContenido> contenido;


    public ContenidoAdapter(Context context, List<ResponseContenido> contenido) {
        this.context = context;
        this.contenido = contenido;
    }

    @Override
    public int getCount() {
        return contenido.size();
    }

    @Override
    public ResponseContenido getItem(int i) {
        return contenido.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getFileFileBankID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_contenido_relacionado, viewGroup, false);
        }

        TextView cont = (TextView) view.findViewById(R.id.contenido);

        final ResponseContenido item = getItem(i);

        cont.setText(item.getNombre());

        return view;
    }

}
