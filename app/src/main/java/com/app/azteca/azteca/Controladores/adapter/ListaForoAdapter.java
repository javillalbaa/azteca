package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.R;

import java.util.List;

public class ListaForoAdapter extends BaseAdapter {

    private Context context;
    private List<Foro> foros;

    public ListaForoAdapter(Context context, List<Foro> foros) {
        this.context = context;
        this.foros = foros;
    }

    @Override
    public int getCount() {
        return foros.size();
    }

    @Override
    public Foro getItem(int i) {
        return foros.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getForoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_foro, viewGroup, false);
        }

        TextView desc = (TextView) view.findViewById(R.id.nombreForoDesc);
        TextView nombre = (TextView) view.findViewById(R.id.nombreForo);
        TextView cantComentarios = (TextView) view.findViewById(R.id.cantComentarios);
        final Foro item = getItem(i);

        String TitForo = "<font color=#424242>" + item.getTema() + "</font>";
        String desc_ = "<font color=#9e9e9e>"+ item.getDescripcion() + "</font>";
        String cantComentarios_ = "<font color=#424242>Comentarios - " + item.getCantidadComentarios().toString() + "</font>";
        nombre.setText(Html.fromHtml(TitForo));
        desc.setText(Html.fromHtml(desc_));
        cantComentarios.setText(Html.fromHtml(cantComentarios_));

        return view;
    }
}