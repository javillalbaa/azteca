package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Subcategoria implements Serializable {

    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("Controller")
    @Expose
    private String controller;
    @SerializedName("Action")
    @Expose
    private String action;
    @SerializedName("Icono")
    @Expose
    private String icono;
    @SerializedName("Orden")
    @Expose
    private int orden;
    @SerializedName("Activo")
    @Expose
    private boolean activo;
    @SerializedName("IdPadre")
    @Expose
    private int idPadre;
    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Subcategorias")
    @Expose
    private Object subcategorias;

    /**
     * No args constructor for use in serialization
     */
    public Subcategoria() {
    }

    /**
     * @param id
     * @param orden
     * @param nombre
     * @param subcategorias
     * @param icono
     * @param idPadre
     * @param action
     * @param controller
     * @param activo
     */
    public Subcategoria(String nombre, String controller, String action, String icono, int orden, boolean activo, int idPadre, int id, Object subcategorias) {
        super();
        this.nombre = nombre;
        this.controller = controller;
        this.action = action;
        this.icono = icono;
        this.orden = orden;
        this.activo = activo;
        this.idPadre = idPadre;
        this.id = id;
        this.subcategorias = subcategorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(int idPadre) {
        this.idPadre = idPadre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(Object subcategorias) {
        this.subcategorias = subcategorias;
    }

}
