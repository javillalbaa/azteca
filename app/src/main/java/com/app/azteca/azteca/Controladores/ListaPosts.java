package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.adapter.ForoPostAdapter;
import com.app.azteca.azteca.Controladores.adapter.ListaForoAdapter;
import com.app.azteca.azteca.Controladores.adapter.ListaPostAdapter;
import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.services.ListaForoService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.widget.AdapterView.OnItemClickListener;

public class ListaPosts extends AppCompatActivity {


    private RecyclerView rvComentarios;
    Retrofit retrofit;
    ArrayList<ForoPost> posts;
    private TextView comentario;

    public ListaPosts(){

      retrofit = new Retrofit.Builder()
             .baseUrl(constant.url)
             .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
             .build();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comentarios_item_list);


        Intent i = getIntent();
        posts = (ArrayList<ForoPost>)i.getSerializableExtra("posts");

        comentario = findViewById(R.id.txv_TituloComentario);
        rvComentarios = findViewById(R.id.rv_listComentarios);

        comentario.setText(Html.fromHtml(i.getExtras().getString("comentario")));
        rvComentarios.setLayoutManager(new LinearLayoutManager(this));
        rvComentarios.setAdapter(new ListaPostAdapter(ListaPosts.this, posts));

    }

    public void Volver(View view) {
        finish();
    }
}
