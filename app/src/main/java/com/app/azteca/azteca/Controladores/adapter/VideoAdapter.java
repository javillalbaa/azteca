package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.MenuResult;
import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.presenters.videos;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class VideoAdapter extends BaseAdapter {

    private Context context;
    private List<videos> video;

    public VideoAdapter(Context context, List<videos> video) {
        this.context = context;
        this.video = video;
    }

    @Override
    public int getCount() {
        return video.size();
    }

    @Override
    public videos getItem(int i) {
        return video.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getVideo();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.videos, viewGroup, false);
        }

        final videos item = getItem(i);
        WebView myWebView = (WebView) view.findViewById(R.id.videoWebView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(item.getUrl());

        return view;
    }
}