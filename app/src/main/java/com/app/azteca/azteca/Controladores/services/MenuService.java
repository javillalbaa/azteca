package com.app.azteca.azteca.Controladores.services;


import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.MenuResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface MenuService {

    @GET("Menu")
    Call<GenericResponse<MenuResult>> getMenu(@Header("Authorization") String token);

}
