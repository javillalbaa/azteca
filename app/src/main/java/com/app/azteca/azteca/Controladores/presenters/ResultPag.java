package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultPag {

    @SerializedName("PaginaActual")
    @Expose
    private int paginaActual;
    @SerializedName("PaginasTotales")
    @Expose
    private int paginasTotales;
    @SerializedName("PageSize")
    @Expose
    private int pageSize;
    @SerializedName("TotalItems")
    @Expose
    private int totalItems;
    @SerializedName("Items")
    @Expose
    private List<Item> items = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResultPag() {
    }

    /**
     *
     * @param paginasTotales
     * @param items
     * @param paginaActual
     * @param totalItems
     * @param pageSize
     */
    public ResultPag(int paginaActual, int paginasTotales, int pageSize, int totalItems, List<Item> items) {
        super();
        this.paginaActual = paginaActual;
        this.paginasTotales = paginasTotales;
        this.pageSize = pageSize;
        this.totalItems = totalItems;
        this.items = items;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }

    public int getPaginasTotales() {
        return paginasTotales;
    }

    public void setPaginasTotales(int paginasTotales) {
        this.paginasTotales = paginasTotales;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
