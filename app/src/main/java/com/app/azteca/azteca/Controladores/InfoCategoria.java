package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.CategoriaAdapter;
import com.app.azteca.azteca.Controladores.adapter.ContenidoAdapter;
import com.app.azteca.azteca.Controladores.adapter.VideoAdapter;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.ResponseContenido;
import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;
import com.app.azteca.azteca.Controladores.presenters.videos;
import com.app.azteca.azteca.Controladores.services.CategoriaService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InfoCategoria extends AppCompatActivity {

    private responseCategoria informacion;
    private ImageView imagen;
    private TextView titulo, desc_corta, subtitulo, autor;
    private List<videos> listaVideos;
    private ListView listview, listCont;
    private VideoAdapter videos;
    private ContenidoAdapter contenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_categoria);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Volver");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imagen = (ImageView) findViewById(R.id.imagen);
        titulo = (TextView) findViewById(R.id.titulo);
        desc_corta = (TextView) findViewById(R.id.desc_corta);
        subtitulo = (TextView) findViewById(R.id.subtitulo);
        autor = (TextView) findViewById(R.id.autor);
        Intent i = getIntent();
        informacion = (responseCategoria)i.getSerializableExtra("inf_categoria");
        MostrarInformacion(informacion);

        listCont.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ResponseContenido item = (ResponseContenido) parent.getItemAtPosition(position);
                Uri uri = Uri.parse(item.getUrlDocumento());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(InfoCategoria.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(InfoCategoria.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void MostrarInformacion(responseCategoria informacion) {

        Glide.with(InfoCategoria.this).load(informacion.getArchivo()).into(imagen);

        titulo.setText(Html.fromHtml(informacion.getTitulo()));
        subtitulo.setText(Html.fromHtml(informacion.getSubTitulo()));
        listaVideos = constant.extractUrls(informacion.getDescripcionLarga());
        videos = new VideoAdapter(getApplicationContext(), listaVideos);
        listview = (ListView) findViewById(R.id.listview);
        listview.getLayoutParams().height = 350 * listaVideos.size();
        listview.setAdapter(videos);
        desc_corta.setText(Html.fromHtml(constant.formaOpenHtml(informacion.getDescripcionLarga())));
        autor.setText("Autor: " + informacion.getUsuariopublica());
        listCont = (ListView) findViewById(R.id.listcont);
        getContenido(informacion.getContenidoID());
    }

    private void getContenido(int id){

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CategoriaService servicescategoria = retrofit.create(CategoriaService.class);
        Call<GenericResponse<ResponseContenido>> call = servicescategoria.getContenido(constant.getToken(), id);

        call.enqueue(new Callback<GenericResponse<ResponseContenido>>() {

            @Override
            public void onResponse(Call<GenericResponse<ResponseContenido>> call, Response<GenericResponse<ResponseContenido>> response) {

                ArrayList<ResponseContenido> listOfStrings = new ArrayList<ResponseContenido>(response.body().getResult().size());
                listOfStrings.addAll(response.body().getResult());
                Log.e("Datos", response.body().getResult().toString());
                if(listOfStrings.size() > 0){
                    TextView textView= findViewById(R.id.Contenido_r);
                    textView.setVisibility(View.VISIBLE);
                    contenido = new ContenidoAdapter(getApplicationContext(), listOfStrings);
                    listCont.getLayoutParams().height = 80 * (listOfStrings.size() + 1);
                    listCont.setAdapter(contenido);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<ResponseContenido>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

}
