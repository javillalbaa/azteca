package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ForoPostService {

    @GET("ForoPost")
    Call<GenericResponse<ForoPost>> getData(@Header("Authorization") String token , @Query("ForoID") Integer ForoID );

    @POST("ForoPost/Create")
    Call<GenericResponse<Boolean>> create(@Header("Authorization") String token , @Body ForoPost Post );


}
