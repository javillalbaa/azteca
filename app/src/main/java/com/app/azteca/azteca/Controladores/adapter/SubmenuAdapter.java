package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.MenuResult;
import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SubmenuAdapter extends BaseAdapter {

    private Context context;
    private List<Subcategoria> submenu;

    public SubmenuAdapter(Context context, List<Subcategoria> submenu) {
        this.context = context;
        this.submenu = submenu;
    }

    @Override
    public int getCount() {
        return submenu.size();
    }

    @Override
    public Subcategoria getItem(int i) {
        return submenu.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_subitem, viewGroup, false);
        }

        ImageView imagen = (ImageView) view.findViewById(R.id.icon);
        TextView nombre = (TextView) view.findViewById(R.id.text);
        final Subcategoria item = getItem(i);
        Bitmap img = constant.imageFromString(item.getIcono(), 100, 100);

        imagen.setImageBitmap(img);
        nombre.setText(item.getNombre());

        return view;
    }
}