package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.Usuario;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface BuscarUsuariosService {

    @GET("UsuarioDirectorio/Directorio")
    Call<GenericResponse<Usuario>> getUsuarios(@Header("Authorization") String token, @Query("ParametroBusqueda") String parametroBusqueda);

}
