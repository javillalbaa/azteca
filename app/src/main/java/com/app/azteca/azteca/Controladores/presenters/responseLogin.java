package com.app.azteca.azteca.Controladores.presenters;

public class responseLogin {

    private String username;
    private String password;

    /**
     * No args constructor for use in serialization
     */
    public responseLogin() {
    }

    /**
     * @param username
     * @param password
     */
    public responseLogin(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

