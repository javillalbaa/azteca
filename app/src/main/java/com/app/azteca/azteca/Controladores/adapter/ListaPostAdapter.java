package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Foro;
import com.app.azteca.azteca.Controladores.presenters.ForoPost;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.services.ForoPostService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListaPostAdapter extends RecyclerView.Adapter<ListaPostViewHolder> {

    private Context context;
    private List<ForoPost> posts;
    RecyclerView rv;
    Retrofit retrofit;

    public ListaPostAdapter(Context context, List<ForoPost> foros) {
        this.context = context;
        this.posts = foros;

        this.retrofit  = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }


    @NonNull
    @Override
    public ListaPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.post_hijo,parent,false);
        return new ListaPostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaPostViewHolder holder, int position) {

        holder.txv_subpost.setText(posts.get(position).getPost());
        holder.txv_postHijoAutor.setText(posts.get(position).getNombresCompletos());
        //holder.txv_postHijoFecha.setText(posts.get(position).getFechaPost());

    }


    @Override
    public int getItemCount() {
        return posts.size();
    }
}