package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.presenters.responseTemas;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;

import java.util.List;

public class TemasAdapter extends BaseAdapter{

    private Context context;
    private List<responseTemas> temas;

    public TemasAdapter(Context context, List<responseTemas> temas) {
        this.context = context;
        this.temas = temas;
    }

    @Override
    public int getCount() {
        return temas.size();
    }

    @Override
    public responseTemas getItem(int i) {
        return temas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getTemaCatalogoID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_temas, viewGroup, false);
        }

        TextView temas = (TextView) view.findViewById(R.id.temas);
        final responseTemas item = getItem(i);

        temas.setText(item.getNombre());

        return view;
    }
}
