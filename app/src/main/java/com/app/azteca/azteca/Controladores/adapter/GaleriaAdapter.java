package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.app.azteca.azteca.Controladores.presenters.Item;
import com.app.azteca.azteca.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class GaleriaAdapter extends BaseAdapter {

    private Context context;
    private List<Item> imagenes;

    public GaleriaAdapter(Context context, List<Item> imagenes) {
        this.context = context;
        this.imagenes = imagenes;
    }

    @Override
    public int getCount() {
        return imagenes.size();
    }

    @Override
    public Item getItem(int i) {
        return imagenes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getImagenTemaID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_galeria, viewGroup, false);
        }

        ImageView imagen = (ImageView) view.findViewById(R.id.imagen);
        final Item item = getItem(i);

        Glide.with(context).load(item.getArchivo()).into(imagen);

        return view;
    }

}
