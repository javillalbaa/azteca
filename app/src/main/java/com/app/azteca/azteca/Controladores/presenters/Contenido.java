package com.app.azteca.azteca.Controladores.presenters;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contenido implements Serializable
{

    @SerializedName("TipoContenido")
    @Expose
    private String tipoContenido;
    @SerializedName("ControllerName")
    @Expose
    private String controllerName;
    @SerializedName("ActionName")
    @Expose
    private String actionName;
    @SerializedName("ParameterName")
    @Expose
    private String parameterName;
    @SerializedName("ParametroID")
    @Expose
    private Integer parametroID;
    @SerializedName("CategoriaContenido")
    @Expose
    private String categoriaContenido;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    private final static long serialVersionUID = 2322653781016015306L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Contenido() {
    }

    /**
     *
     * @param controllerName
     * @param parametroID
     * @param tipoContenido
     * @param descripcion
     * @param actionName
     * @param categoriaContenido
     * @param parameterName
     */
    public Contenido(String tipoContenido, String controllerName, String actionName, String parameterName, Integer parametroID, String categoriaContenido, String descripcion) {
        super();
        this.tipoContenido = tipoContenido;
        this.controllerName = controllerName;
        this.actionName = actionName;
        this.parameterName = parameterName;
        this.parametroID = parametroID;
        this.categoriaContenido = categoriaContenido;
        this.descripcion = descripcion;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public Integer getParametroID() {
        return parametroID;
    }

    public void setParametroID(Integer parametroID) {
        this.parametroID = parametroID;
    }

    public String getCategoriaContenido() {
        return categoriaContenido;
    }

    public void setCategoriaContenido(String categoriaContenido) {
        this.categoriaContenido = categoriaContenido;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
