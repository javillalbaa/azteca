package com.app.azteca.azteca.Controladores.presenters;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Usuario implements Serializable
{

    @SerializedName("UsuarioID")
    @Expose
    private Integer usuarioID;
    @SerializedName("NombreUsuario")
    @Expose
    private String nombreUsuario;
    @SerializedName("CorreoElectronico")
    @Expose
    private String correoElectronico;
    @SerializedName("PrimerNombre")
    @Expose
    private String primerNombre;
    @SerializedName("PrimerApellido")
    @Expose
    private String primerApellido;
    @SerializedName("NombreCompleto")
    @Expose
    private String nombreCompleto;
    @SerializedName("Documento")
    @Expose
    private String documento;
    @SerializedName("Extension")
    @Expose
    private String extension;
    @SerializedName("CelularCorporativo")
    @Expose
    private String celularCorporativo;
    private final static long serialVersionUID = -7471234784902102141L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Usuario() {
    }

    /**
     *
     * @param extension
     * @param correoElectronico
     * @param celularCorporativo
     * @param primerApellido
     * @param documento
     * @param nombreCompleto
     * @param usuarioID
     * @param primerNombre
     * @param nombreUsuario
     */
    public Usuario(Integer usuarioID, String nombreUsuario, String correoElectronico, String primerNombre, String primerApellido, String nombreCompleto, String documento, String extension, String celularCorporativo) {
        super();
        this.usuarioID = usuarioID;
        this.nombreUsuario = nombreUsuario;
        this.correoElectronico = correoElectronico;
        this.primerNombre = primerNombre;
        this.primerApellido = primerApellido;
        this.nombreCompleto = nombreCompleto;
        this.documento = documento;
        this.extension = extension;
        this.celularCorporativo = celularCorporativo;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getCelularCorporativo() {
        return celularCorporativo;
    }

    public void setCelularCorporativo(String celularCorporativo) {
        this.celularCorporativo = celularCorporativo;
    }

}