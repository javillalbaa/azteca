package com.app.azteca.azteca.Controladores;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.app.azteca.azteca.Controladores.adapter.ComentariosComCarrosAdapter;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.ResponseComComentarioCarro;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseCompartirCarro;
import com.app.azteca.azteca.Controladores.services.CompartirCarroService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Field;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InfCompartirCarro extends AppCompatActivity implements ComentariosComCarrosAdapter.AdapterCompartirCarroPostCallback {

    RecyclerView rv;
    private ListView listview;
    private responseCompartirCarro compartirCarro;
    private TextView informacion;
    Retrofit retrofit;
    private ProgressBar simpleProgressBar;
    private ViewGroup row;

    private ArrayList<ResponseComComentarioCarro> comentarios;
    private ComentariosComCarrosAdapter adapterComentarios;

    public InfCompartirCarro() {

        retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inf_compartir_carro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Información");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        simpleProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        informacion = (TextView) findViewById(R.id.informacion);

        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));

        Intent i = getIntent();
        compartirCarro = (responseCompartirCarro)i.getSerializableExtra("infCompartirCarro");
        mostrarInf(compartirCarro);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(InfCompartirCarro.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(InfCompartirCarro.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void mostrarInf(responseCompartirCarro datos){
        String informacion_ = "<font color=#424242>Destino:</font><font color=#9e9e9e>"+ datos.getDestino() + "</font><br>";
        informacion_ += "<font color=#424242>Nombre Contacto:</font><font color=#9e9e9e>"+ datos.getNombreContacto() + "</font><br>";
        informacion_ += "<font color=#424242>Teléfono Contacto:</font><font color=#9e9e9e>"+ datos.getTelefonoContacto() + "</font><br>";
        informacion_ += "<font color=#424242>Fecha y Hora de Salida:</font><font color=#9e9e9e>" + datos.getFechaSalida().substring(0, 10) + " " + datos.getHoraSalida() + "</font><br>";
        informacion_ += "<font color=#424242>Punto de Encuentro:</font><font color=#9e9e9e>" + datos.getPuntoEncuentro() + "</font><br>";
        informacion_ += "<font color=#424242>Descripción de la Ruta:</font><font color=#9e9e9e>" + datos.getDescripcion() + "</font>";
        informacion.setText(Html.fromHtml(informacion_));
        ListaComentarios();
    }

    public void ListaComentarios(){
        CompartirCarroService service = retrofit.create(CompartirCarroService.class);
        Call<GenericResponse<ResponseComComentarioCarro>> call = service.getComentarios(constant.getToken(), compartirCarro.getCompartirCarroID());
        call.enqueue(new Callback<GenericResponse<ResponseComComentarioCarro>>() {

            @Override
            public void onResponse(Call<GenericResponse<ResponseComComentarioCarro>> call, Response<GenericResponse<ResponseComComentarioCarro>> response) {

                try{
                    if( response.body().getMessage().getCode() == 200 ) {

                        ArrayList<ResponseComComentarioCarro> listOfStrings = new ArrayList<ResponseComComentarioCarro>(response.body().getResult().size());
                        listOfStrings.addAll(response.body().getResult());
                        comentarios = listOfStrings;
                        simpleProgressBar.setVisibility(View.GONE);
                        rv.setAdapter(new ComentariosComCarrosAdapter(InfCompartirCarro.this, comentarios));

                    }else{
                        //new Toast(getApplicationContext()).setText("Error cargando comentarío");
                    }
                } catch (Exception e) {
                    Toast.makeText(InfCompartirCarro.this,
                            "No se pudo cargar los comentario",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<ResponseComComentarioCarro>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());
            }
        });
    }

    public void crearComentario(View view) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(InfCompartirCarro.this);
        View mView = getLayoutInflater().inflate(R.layout.comentario, null);
        final EditText comentario = (EditText) mView.findViewById(R.id.comentario);
        FloatingActionButton mLogin = (FloatingActionButton) mView.findViewById(R.id.btnLogin);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!comentario.getText().toString().isEmpty()){
                    simpleProgressBar.setVisibility(View.VISIBLE);
                    crearComentarioApi(comentario.getText().toString());
                    dialog.dismiss();
                }else{
                    Toast.makeText(InfCompartirCarro.this,
                            "El comentario no puede ser vacio",
                            Toast.LENGTH_SHORT).show();
                }
            }
            });
    }

    public void crearComentarioApi(String comentario){

        ResponseComComentarioCarro nuevo_comentario = new ResponseComComentarioCarro();
        nuevo_comentario.setPost(comentario);
        nuevo_comentario.setCompartirCarroID(compartirCarro.getCompartirCarroID());
        nuevo_comentario.setUsuarioID(constant.getIdUser());
        CompartirCarroService service = retrofit.create(CompartirCarroService.class);
        Call<GenericResponse<Boolean>> call = service.create(constant.getToken(), nuevo_comentario );

        call.enqueue(new Callback<GenericResponse<Boolean>>() {

            @Override
            public void onResponse(Call<GenericResponse<Boolean>> call, Response<GenericResponse<Boolean>> response) {

                try{
                    if( response.body().getMessage().getCode() == 200 ) {
                        Boolean b = response.body().getResult().get(0);
                        if(b)
                            ListaComentarios();
                        else
                            new Toast(getApplicationContext()).setText("Error creando comentarío");
                    }else{
                        new Toast(getApplicationContext()).setText("Error creando comentarío");
                    }
                } catch (Exception e) {
                    Toast.makeText(InfCompartirCarro.this,
                            "No se pudo envíar el comentario",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GenericResponse<Boolean>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }
        });
    }

    @Override
    public void recargarComentarios() {
        ListaComentarios();
    }
}
