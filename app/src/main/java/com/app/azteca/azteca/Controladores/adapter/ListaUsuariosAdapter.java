package com.app.azteca.azteca.Controladores.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.Controladores.presenters.Usuario;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;

import java.util.List;

public class ListaUsuariosAdapter extends BaseAdapter {

    private Context context;
    private List<Usuario> usuarios;

    public ListaUsuariosAdapter(Context context, List<Usuario> usuarios) {
        this.context = context;
        this.usuarios = usuarios;
    }

    @Override
    public int getCount() {
        return usuarios.size();
    }

    @Override
    public Usuario getItem(int i) {
        return usuarios.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getUsuarioID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_usuario, viewGroup, false);
        }

        TextView nombre = view.findViewById(R.id.textviewNombreUsuario);
        TextView correo =view.findViewById(R.id.textviewCorreoUsuario);
        TextView telefono = view.findViewById(R.id.textviewTelefonoUsuario);
        final Usuario item = getItem(i);

        nombre.setText(Html.fromHtml("<b>Nombre</b>: "+item.getNombreCompleto()));
        correo.setText(item.getCorreoElectronico().trim().equals("")? Html.fromHtml("<b>Correo</b>: No asignado") : Html.fromHtml("<b>Correo</b>: "+item.getCorreoElectronico()));
        telefono.setText(item.getCelularCorporativo().trim().equals("")? Html.fromHtml("<b>Telefono</b>: No asignado") : Html.fromHtml("<b>Telefono</b>: "+item.getCelularCorporativo()));
        return view;
    }
}
