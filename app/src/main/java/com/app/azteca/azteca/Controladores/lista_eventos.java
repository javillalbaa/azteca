package com.app.azteca.azteca.Controladores;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.app.azteca.azteca.Controladores.adapter.listaEventosAdapter;
import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.R;

import java.util.List;


public class lista_eventos extends Fragment {
    listaEventosAdapter listaEventosAdapter;
    public lista_eventos() {

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        List<Evento> eventos=(List<Evento>) getArguments().getSerializable("eventos");

        View view = inflater.inflate(R.layout.fragment_lista_eventos,
                container, false);
        ListView listView=view.findViewById(R.id.listaeventos);

        listaEventosAdapter=new listaEventosAdapter(container.getContext(),eventos);
        listView.setAdapter(listaEventosAdapter);
        return view;
    }

}
