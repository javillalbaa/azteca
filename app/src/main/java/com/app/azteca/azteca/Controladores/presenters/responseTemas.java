package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class responseTemas {

    @SerializedName("TemaCatalogoID")
    @Expose
    private int temaCatalogoID;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;

    public responseTemas(int temaCatalogoID, String nombre, String descripcion) {
        this.temaCatalogoID = temaCatalogoID;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTemaCatalogoID() {
        return temaCatalogoID;
    }

    public void setTemaCatalogoID(int temaCatalogoID) {
        this.temaCatalogoID = temaCatalogoID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
