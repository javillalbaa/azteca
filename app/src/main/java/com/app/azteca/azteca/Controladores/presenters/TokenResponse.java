package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenResponse {
    @SerializedName("Result")
    @Expose
    private List<MenuResult> result = null;
    @SerializedName("Message")
    @Expose
    private Message message;

    /**
     * No args constructor for use in serialization
     */
    public TokenResponse() {
    }

    /**
     * @param message
     * @param result
     */
    public TokenResponse(List<MenuResult> result, Message message) {
        super();
        this.result = result;
        this.message = message;
    }

    public List<MenuResult> getResult() {
        return result;
    }

    public void setResult(List<MenuResult> result) {
        this.result = result;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}
