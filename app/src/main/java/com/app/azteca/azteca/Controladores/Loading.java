package com.app.azteca.azteca.Controladores;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.app.azteca.azteca.R;

public class Loading extends Fragment {

    public Loading() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading,
                container, false);
        ImageView imageView= view.findViewById(R.id.logoLoading);
        Animation animation=AnimationUtils.loadAnimation(getContext(),R.anim.rotate);
        animation.setRepeatCount(Animation.INFINITE);
        imageView.startAnimation(animation);
        imageView.getAnimation().setRepeatCount(Animation.INFINITE);
        return view;
    }

}
