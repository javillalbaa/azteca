package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.presenters.responseLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginService {

    @Headers({"Content-Type: application/json"})
    @POST("Account")
    Call<TokenResponse> getToken(@Body responseLogin body);

    @POST("Account/Logout")
    Call<TokenResponse> exit(@Header("Authorization") String token);

}
