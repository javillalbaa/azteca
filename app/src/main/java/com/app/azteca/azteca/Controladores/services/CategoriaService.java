package com.app.azteca.azteca.Controladores.services;

import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.ResponseContenido;
import com.app.azteca.azteca.Controladores.presenters.responseCategoria;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface CategoriaService {

    @GET("Contenido/GetByCategoryId")
    Call<GenericResponse<responseCategoria>> getCategorias(@Header("Authorization") String token, @Query("id") Integer id);

    @GET("ContenidoDocumento")
    Call<GenericResponse<ResponseContenido>> getContenido(@Header("Authorization") String token, @Query("ContenidoID") Integer id);

}
