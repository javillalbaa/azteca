package com.app.azteca.azteca.Controladores.variablesConstant;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Base64;

import com.app.azteca.azteca.Controladores.presenters.Subcategoria;
import com.app.azteca.azteca.Controladores.presenters.videos;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class constant {

    private static String Token = "";
    //private static String TokenStatus = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJUb2tlbklEIjoiMzVkZDQwNzEtMDU1MS00NWRmLWJhODMtMjMyNzA1N2JiYTcyIiwiVXNlcklEIjoiODU5OCIsIlVzZXJOYW1lIjoiZXhkZm9uc2VjYSIsIm5iZiI6MTUzOTcxNzE2NiwiZXhwIjoxNTQyMzA5MTY2LCJpYXQiOjE1Mzk3MTcxNjZ9.6pOB4-SQi0xq1EFLKt089MnfuVnZVfZSPFKn9iyuFpg";
    private static int id_categoria;
    //private static int idUser = 8598;
    private static int idUser = -1;
    private static List<Subcategoria> subcategoria = new ArrayList<>();
    public static String getToken() { return Token; }
    public static int getIdUser() { return idUser; }
    public static int getIdCategoria() { return id_categoria; }
    public static List<Subcategoria> getSubcategoria() { return subcategoria; }

    public static final String url = "http://orion.azteca-comunicaciones.com/ConexionAzApi/api/";
    public static final String urlPruebas = "localhost:25282/api/";

    public static Bitmap imageFromString(String imageData, int width, int hegth)
    {
        String data = imageData.substring(imageData.indexOf(",") + 1);
        byte[] imageAsBytes = Base64.decode(data.getBytes(), Base64.DEFAULT);
        String  svgAsString = new String(imageAsBytes, Charset.forName("UTF-8"));

        SVG svg = null;
        try {
            svg = SVG.getFromString(svgAsString);
        } catch (SVGParseException e) {
            e.printStackTrace();
        }

        // Create a bitmap and canvas to draw onto
        float   svgWidth = (svg.getDocumentWidth() != -1) ? svg.getDocumentWidth() : 500f;
        float   svgHeight = (svg.getDocumentHeight() != -1) ? svg.getDocumentHeight() : 500f;

        Bitmap  newBM = Bitmap.createBitmap(width,
                hegth,
                Bitmap.Config.ARGB_8888);
        Canvas bmcanvas = new Canvas(newBM);

        // Clear background to white if you want
        bmcanvas.drawRGB(255, 255, 255);

        // Render our document onto our canvas
        svg.renderToCanvas(bmcanvas);

        return newBM;
    }

    public static Bitmap imageFromStringCategory(String imageData)
    {
        String data = imageData.substring(imageData.indexOf(",") + 1);
        byte[] imageAsBytes = Base64.decode(data.getBytes(), Base64.DEFAULT);
        //String  svgAsString = new String(imageAsBytes, Charset.forName("UTF-8"));

        Bitmap decodedByte = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        return decodedByte;
    }

    public static String formaOpenHtml(String html){
        String html_ = html.replace('[','<');
        return html_.replace(']', '>');
    }

    public static List<videos> extractUrls(String text)
    {
        List<videos> lista = new ArrayList<>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        int i = 1;
        while (urlMatcher.find())
        {
            String url = text.substring(urlMatcher.start(0), urlMatcher.end(0));
            lista.add(new videos(i,url, " ver video " + Integer.toString(i)));
            i++;
        }

        return lista;
    }

}
