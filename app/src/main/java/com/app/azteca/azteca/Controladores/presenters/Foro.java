package com.app.azteca.azteca.Controladores.presenters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Foro implements Serializable
{
    @SerializedName("ForoID")
    @Expose
    private Integer foroID;
    @SerializedName("CantidadComentarios")
    @Expose
    private Integer cantidadComentarios;
    @SerializedName("NuevosComentarios")
    @Expose
    private Integer nuevosComentarios;
    @SerializedName("ForoPostID")
    @Expose
    private Integer foroPostID;
    @SerializedName("FechaInicio")
    @Expose
    private String fechaInicio;
    @SerializedName("FechaFin")
    @Expose
    private String fechaFin;
    @SerializedName("FechaPost")
    @Expose
    private String fechaPost;
    @SerializedName("FechaInicioFormat")
    @Expose
    private String fechaInicioFormat;
    @SerializedName("FechaFinFormat")
    @Expose
    private String fechaFinFormat;
    @SerializedName("FechaPostFormat")
    @Expose
    private String fechaPostFormat;
    @SerializedName("Tema")
    @Expose
    private String tema;
    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("Activo")
    @Expose
    private Boolean activo;
    @SerializedName("Post")
    @Expose
    private String post;
    @SerializedName("CalificacionLike")
    @Expose
    private Integer calificacionLike;
    @SerializedName("CalificacionDisLike")
    @Expose
    private Integer calificacionDisLike;
    @SerializedName("UsuarioID")
    @Expose
    private Integer usuarioID;
    @SerializedName("NombresCompletos")
    @Expose
    private String nombresCompletos;


    /**
     * No args constructor for use in serialization
     *
     */
    public Foro() {
    }

    /**
     *
     * @param post
     * @param nuevosComentarios
     * @param fechaPost
     * @param descripcion
     * @param fechaFin
     * @param tema
     * @param cantidadComentarios
     * @param usuarioID
     * @param activo
     * @param foroPostID
     * @param fechaFinFormat
     * @param foroID
     * @param fechaInicioFormat
     * @param nombresCompletos
     * @param fechaPostFormat
     * @param calificacionDisLike
     * @param calificacionLike
     * @param fechaInicio
     */
    public Foro(Integer foroID, Integer cantidadComentarios, Integer nuevosComentarios, Integer foroPostID, String fechaInicio, String fechaFin, String fechaPost, String fechaInicioFormat, String fechaFinFormat, String fechaPostFormat, String tema, String descripcion, Boolean activo, String post, Integer calificacionLike, Integer calificacionDisLike, Integer usuarioID, String nombresCompletos) {
        super();
        this.foroID = foroID;
        this.cantidadComentarios = cantidadComentarios;
        this.nuevosComentarios = nuevosComentarios;
        this.foroPostID = foroPostID;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaPost = fechaPost;
        this.fechaInicioFormat = fechaInicioFormat;
        this.fechaFinFormat = fechaFinFormat;
        this.fechaPostFormat = fechaPostFormat;
        this.tema = tema;
        this.descripcion = descripcion;
        this.activo = activo;
        this.post = post;
        this.calificacionLike = calificacionLike;
        this.calificacionDisLike = calificacionDisLike;
        this.usuarioID = usuarioID;
        this.nombresCompletos = nombresCompletos;
    }

    public Integer getForoID() {
        return foroID;
    }

    public void setForoID(Integer foroID) {
        this.foroID = foroID;
    }

    public Integer getCantidadComentarios() {
        return cantidadComentarios;
    }

    public void setCantidadComentarios(Integer cantidadComentarios) {
        this.cantidadComentarios = cantidadComentarios;
    }

    public Integer getNuevosComentarios() {
        return nuevosComentarios;
    }

    public void setNuevosComentarios(Integer nuevosComentarios) {
        this.nuevosComentarios = nuevosComentarios;
    }

    public Integer getForoPostID() {
        return foroPostID;
    }

    public void setForoPostID(Integer foroPostID) {
        this.foroPostID = foroPostID;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getFechaPost() {
        return fechaPost;
    }

    public void setFechaPost(String fechaPost) {
        this.fechaPost = fechaPost;
    }

    public String getFechaInicioFormat() {
        return fechaInicioFormat;
    }

    public void setFechaInicioFormat(String fechaInicioFormat) {
        this.fechaInicioFormat = fechaInicioFormat;
    }

    public String getFechaFinFormat() {
        return fechaFinFormat;
    }

    public void setFechaFinFormat(String fechaFinFormat) {
        this.fechaFinFormat = fechaFinFormat;
    }

    public String getFechaPostFormat() {
        return fechaPostFormat;
    }

    public void setFechaPostFormat(String fechaPostFormat) {
        this.fechaPostFormat = fechaPostFormat;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Integer getCalificacionLike() {
        return calificacionLike;
    }

    public void setCalificacionLike(Integer calificacionLike) {
        this.calificacionLike = calificacionLike;
    }

    public Integer getCalificacionDisLike() {
        return calificacionDisLike;
    }

    public void setCalificacionDisLike(Integer calificacionDisLike) {
        this.calificacionDisLike = calificacionDisLike;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNombresCompletos() {
        return nombresCompletos;
    }

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

}