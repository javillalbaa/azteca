package com.app.azteca.azteca.Controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.azteca.azteca.Controladores.adapter.EventoAdpater;
import com.app.azteca.azteca.Controladores.presenters.Evento;
import com.app.azteca.azteca.Controladores.presenters.EventoCalendar;
import com.app.azteca.azteca.Controladores.presenters.GenericResponse;
import com.app.azteca.azteca.Controladores.presenters.TokenResponse;
import com.app.azteca.azteca.Controladores.services.EventosService;
import com.app.azteca.azteca.Controladores.services.LoginService;
import com.app.azteca.azteca.Controladores.variablesConstant.constant;
import com.app.azteca.azteca.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.CalendarWeekDayFormatter;
import com.prolificinteractive.materialcalendarview.format.DateFormatDayFormatter;
import com.prolificinteractive.materialcalendarview.format.DayFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Eventos extends AppCompatActivity implements OnDateSelectedListener {
    MaterialCalendarView calendarView;
    EventoAdpater evento;
    List<CalendarDay> dates;
    List<Evento> eventos;
    List<Evento> eventosFiltrados;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dates= new ArrayList<>();
        eventos=new ArrayList<>();
        eventosFiltrados=new ArrayList<>();
        setContentView(R.layout.activity_eventos);
        calendarView =findViewById(R.id.calendarView);
        CharSequence s[]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"};
        calendarView.setTitleMonths(s);
        calendarView.setWeekDayFormatter(new WeekDayFormatter() {
            @Override
            public CharSequence format(int i) {
                return i==1?"Lun":i==2?"Mar":i==3?"Mie":i==4?"Jue":i==5?"Vie":i==6?"Sab":"Dom";
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Eventos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        calendarView.setOnDateChangedListener(this);
        getEventos();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu sub_menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, (android.view.Menu) sub_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                Intent intent = new Intent(getApplicationContext(), com.app.azteca.azteca.Controladores.Menu.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_exit:

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(constant.url)
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                        .build();
                LoginService exit = retrofit.create(LoginService.class);

                Call<TokenResponse> call = exit.exit(constant.getToken());
                call.enqueue(new Callback<TokenResponse>() {

                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if( response.body().getMessage().getMessage().equals("Ha salido de la aplicación exitosamente")){
                            Field field = null;
                            try {
                                field = constant.class.getDeclaredField("Token");
                                field.setAccessible(true);
                                field.set(null, "");
                                Field field_id = constant.class.getDeclaredField("idUser");
                                field_id.setAccessible(true);
                                field_id.set(null, -1);
                                Intent intent = new Intent(Eventos.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(Eventos.this,
                                        response.body().getMessage().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Log.e("Error test:   ", t.toString());
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getEventos(){

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(constant.url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        EventosService servicesevento = retrofit.create(EventosService.class);
        Call<GenericResponse<Evento>> call = servicesevento.getData(constant.getToken());

        call.enqueue(new Callback<GenericResponse<Evento>>() {

            @Override
            public void onResponse(Call<GenericResponse<Evento>> call, Response<GenericResponse<Evento>> response) {
                Log.d("Datos:",response.body().toString());
                eventos=response.body().getResult();
                evento = new EventoAdpater(getApplicationContext(), response.body().getResult());
                for (Evento evento:response.body().getResult()) {
                    SimpleDateFormat f= new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date=f.parse(evento.getFechaEvento());
                        dates.add(CalendarDay.from(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                EventoCalendar event= new EventoCalendar(R.color.primaryColor,dates);
                calendarView.addDecorator(event);
            }

            @Override
            public void onFailure(Call<GenericResponse<Evento>> call, Throwable t) {
                Log.e("Error test:   ", t.toString());

            }

        });
    }
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
        Date d= calendarDay.getDate();
        eventosFiltrados=new ArrayList<>();
        for (Evento evento:eventos) {
            SimpleDateFormat f= new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date=f.parse(evento.getFechaEvento());
                if(d.getTime()==date.getTime()){
                    eventosFiltrados.add(evento);
                }else{
                    Toast.makeText(getApplicationContext(),"No hay eventos",Toast.LENGTH_SHORT);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(getApplicationContext(), ListaEventos.class);
        intent.putExtra("eventos",(Serializable) eventosFiltrados);
        intent.putExtra("date",calendarDay.getDay()+"-"+(calendarDay.getMonth()+1)+"-"+calendarDay.getYear());
        Log.d("Datos: ", "Prueba" );
        startActivity(intent);
    }
}
